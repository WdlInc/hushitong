# Used Access JPA Data with Rest:
https://spring.io/guides/gs/accessing-data-rest/
https://docs.spring.io/spring-data/rest/docs/current/reference/html/

# For development, H2 database is used. In production, MariaDB will be used with changes in pom.xml and application.yml

# For testing:
cd D:/private/fst/opt
rm -rf datarest
tar xvf ../hushitong/datarest/target/datarest-bin.tar.gz
./datarest/launchServer.sh

curl http://localhost:8088/data
curl -l -H "Content-type: application/json" -X POST -d '{"instName":"机构A","description":"李村"}' http://localhost:8088/data/institution

http://http://localhost:8088
http://http://localhost:8088/device

device (parent)      person (child)
+------------+       +------------+
| *device_id |       | *id        |
| person     | <---> | *device_id |
| institution|       | institution|
+------------+       +------------+
      |                    |
      |                    |
      +----------+---------+
                 |
                 V
           +------------- +
           |     name     |
           +--------------+
           Institution

mysql -u root -p
r00t
> select user,host from mysql.user;
> create user wdl identified by 'wdl4hushitong';
> #drop user wdl;
> grant all on *.* to 'wdl'@'%';
> flush privileges;
> create database db4keycloak;
> create database hstdb;

> show databases;

curl -i -X PUT -H "Content-Type:text/uri-list" -d "http://localhost:8088/data/institution/2" http://localhost:8088/data/building/1/institution


# Add inst helloSBC
curl -H "Content-Type:application/json" -X POST -d '{"instName": "helloSBC", "username": "admin"}' http://localhost:8088/data/institution

# add new alarm UP
curl -H "Content-Type:application/json" -X POST -d '{"institutionId": 1, "type": "AWAY", "raisedTime": 1541075024000, "personName": "SBC", "deviceId": "DEVICE001", "personId": 1}' http://localhost:8088/data/alarm
curl -H "Content-Type:application/json" -X POST -d '{"institutionId": 1, "type": "MOVE", "raisedTime": 1537507739183, "personName": "张三", "deviceId": "DEVICE001", "personId": 1}' http://localhost:8088/data/alarm

# Update person alarm flag
curl -H "Content-Type:application/json" -X PATCH -d '{"upAlarmFlag": true}' http://localhost:8088/data/person/1
curl -H "Content-Type:application/json" -X PATCH -d '{"moveAlarmFlag": true}' http://localhost:8088/data/person/1


# For prometheus
http://localhost:8088/prometheus

# view prometheus targets
http://localhost:9090/targets

http://localhost:9090/api/v1/query_range?query=breathe&start=1539346541.837&end=1539350141.837&step=14&_=1539350088611


// query the instant value with personId equals to 1, it will return the most recent value within 5 minutes, it could be empty if no data within 5 minutes!
http://localhost:9090/api/v1/query?query=move{personId="1"}
{
  status: "success"
  data.result[0].metric{__name__: "breathe", personId: "1"}
  data.result[0].value[1539351117.772, "0"]
}

// get delta for last 10 seconds
http://localhost:9090/api/v1/query?query=round(delta(move{personId="1"}[10s]))

// query range value for last 5 minutes => s, m, h, d (days), w (weeks), y (years)
http://localhost:9090/api/v1/query?query=breathe{personId="1"}[5m]
{data.result[0].values[
 [1539351373.978, "14"],
 [1539351378.987, "17"],
]}

// range query
http://localhost:9090/api/v1/query_range?query=breathe{personId="1"}&start=1539346541.837&end=1539350141.837&step=14


// For elasticsearch
curl localhost:9200/_cat/indices?v
curl -XDELETE localhost:9200/hst.hist.0


// some notes about mysql
use hstdb;
describe table person;
alter table person drop turn_over_reminder_alarm_id;
alter table person drop spare1,drop spare2,drop spare3,drop spare4,drop spare5;
alter table person drop spare6, drop spare7,drop spare8,drop spare9,drop spare10;

