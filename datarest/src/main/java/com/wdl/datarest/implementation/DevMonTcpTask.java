package com.wdl.datarest.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.wdl.datarest.data.DeviceRepository;
import com.wdl.datarest.data.PersonRepository;

@Component
@Order(2)
public class DevMonTcpTask implements ApplicationRunner {

	@Autowired
	private PersonRepository personRepo;

	@Autowired
	private DevCnfgCache devCnfgCache;

	@Autowired
	private InfluxDBData influxDBData;

	@Autowired
	private DeviceRepository deviceRepo;

	private final int PORT_NUMBER2 = 10188;
	
	private final int DEV_DATA_THREAD_NUM = 20;
	private DevWorkerThread[] devDataThreads = new DevWorkerThread[DEV_DATA_THREAD_NUM];
	
	@Autowired
	private DevWorkerThread devDataThread0;

	@Autowired
	private DevWorkerThread devDataThread1;

	@Autowired
	private DevWorkerThread devDataThread2;

	@Autowired
	private DevWorkerThread devDataThread3;

	@Autowired
	private DevWorkerThread devDataThread4;

	@Autowired
	private DevWorkerThread devDataThread5;

	@Autowired
	private DevWorkerThread devDataThread6;

	@Autowired
	private DevWorkerThread devDataThread7;

	@Autowired
	private DevWorkerThread devDataThread8;

	@Autowired
	private DevWorkerThread devDataThread9;

	@Autowired
	private DevWorkerThread devDataThread10;

	@Autowired
	private DevWorkerThread devDataThread11;

	@Autowired
	private DevWorkerThread devDataThread12;

	@Autowired
	private DevWorkerThread devDataThread13;

	@Autowired
	private DevWorkerThread devDataThread14;

	@Autowired
	private DevWorkerThread devDataThread15;

	@Autowired
	private DevWorkerThread devDataThread16;

	@Autowired
	private DevWorkerThread devDataThread17;

	@Autowired
	private DevWorkerThread devDataThread18;

	@Autowired
	private DevWorkerThread devDataThread19;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		
		devDataThread0.setThreadId(0);
		devDataThreads[0] = devDataThread0;
		devDataThreads[0].start();

		devDataThread1.setThreadId(1);
		devDataThreads[1] = devDataThread1;
		devDataThreads[1].start();

		devDataThread2.setThreadId(2);
		devDataThreads[2] = devDataThread2;
		devDataThreads[2].start();

		devDataThread3.setThreadId(3);
		devDataThreads[3] = devDataThread3;
		devDataThreads[3].start();

		devDataThread4.setThreadId(4);
		devDataThreads[4] = devDataThread4;
		devDataThreads[4].start();

		devDataThread5.setThreadId(5);
		devDataThreads[5] = devDataThread5;
		devDataThreads[5].start();

		devDataThread6.setThreadId(6);
		devDataThreads[6] = devDataThread6;
		devDataThreads[6].start();

		devDataThread7.setThreadId(7);
		devDataThreads[7] = devDataThread7;
		devDataThreads[7].start();

		devDataThread8.setThreadId(8);
		devDataThreads[8] = devDataThread8;
		devDataThreads[8].start();

		devDataThread9.setThreadId(9);
		devDataThreads[9] = devDataThread9;
		devDataThreads[9].start();

		devDataThread10.setThreadId(10);
		devDataThreads[10] = devDataThread10;
		devDataThreads[10].start();

		devDataThread11.setThreadId(11);
		devDataThreads[11] = devDataThread11;
		devDataThreads[11].start();

		devDataThread12.setThreadId(12);
		devDataThreads[12] = devDataThread12;
		devDataThreads[12].start();

		devDataThread13.setThreadId(13);
		devDataThreads[13] = devDataThread13;
		devDataThreads[13].start();

		devDataThread14.setThreadId(14);
		devDataThreads[14] = devDataThread14;
		devDataThreads[14].start();

		devDataThread15.setThreadId(15);
		devDataThreads[15] = devDataThread15;
		devDataThreads[15].start();

		devDataThread16.setThreadId(16);
		devDataThreads[16] = devDataThread16;
		devDataThreads[16].start();

		devDataThread17.setThreadId(17);
		devDataThreads[17] = devDataThread17;
		devDataThreads[17].start();

		devDataThread18.setThreadId(18);
		devDataThreads[18] = devDataThread18;
		devDataThreads[18].start();

		devDataThread19.setThreadId(19);
		devDataThreads[19] = devDataThread19;
		devDataThreads[19].start();
		
		
		new DevMonTcpServer(PORT_NUMBER2, devCnfgCache, personRepo, influxDBData, deviceRepo,devDataThreads);
	}

}
