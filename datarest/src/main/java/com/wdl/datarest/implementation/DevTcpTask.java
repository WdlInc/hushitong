package com.wdl.datarest.implementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.wdl.datarest.data.DeviceRepository;
import com.wdl.datarest.data.PersonRepository;

@Component
@Order(1)
public class DevTcpTask implements ApplicationRunner {

	@Autowired
	private PersonRepository personRepo;

	@Autowired
	private DevCnfgCache devCnfgCache;

	@Autowired
	private InfluxDBData influxDBData;

	@Autowired
	private DeviceRepository deviceRepo;
	@Autowired
	private ShouHuanDataCache shouHuanDataCache;

	private final int PORT_NUMBER = 10177;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		new Server(PORT_NUMBER, devCnfgCache, personRepo, influxDBData, deviceRepo, shouHuanDataCache);
	}

}
