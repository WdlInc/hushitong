package com.wdl.datarest.implementation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wdl.datarest.data.Phone;
import com.wdl.datarest.data.PhoneRepository;

@RestController
@RequestMapping("/phb")
public class PhoneBookController {
	@Autowired
	private PhoneRepository phoneRepo;

	@GetMapping(value = "/setPhoneBook")
	public String setPhoneBook(@RequestParam("devId") String devId, HttpServletRequest request) {
		try {
			System.out.println("设备id：" + devId);

			String devId2;
			if (devId.startsWith("20CH")) {
				devId2 = devId.substring(4);
			} else {
				devId2 = devId;
			}
			System.out.println("去前缀后的devId：" + devId2);
			List<Phone> p = phoneRepo.findByDevId("20CH" + devId2);

			if (p == null || p.isEmpty()) {
				return "2";
			}
			Phone phone = p.get(0);
			System.out.println("phonebook:" + phone.getPhoneBook());

			String[] sArr = phone.getPhoneBook().split(",");
			String[] phoneArr = new String[3];
			phoneArr[0] = "00000000000";
			phoneArr[1] = "00000000000";
			phoneArr[2] = "00000000000";
			if (sArr != null && sArr.length >= 6) {
				for (int i = 0; i < 3; i++) {
					phoneArr[i] = sArr[i * 2];
				}
			} else if (sArr != null && sArr.length == 2) {
				phoneArr[0] = sArr[0];
			} else if (sArr != null && sArr.length == 4) {
				phoneArr[0] = sArr[0];
				phoneArr[1] = sArr[2];
			}

			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < phoneArr.length; i++) {
				builder.append(phoneArr[i]);
				if (i != phoneArr.length - 1) {
					builder.append(",");
				}
			}

			String msg = "[3g*" + devId2 + "*0027*SOS," + builder.toString() + "]";
			System.out.println(msg);
			NettyConfig.sendMessage(devId2, msg);

		} catch (Exception e) {
			e.printStackTrace();
			return "3";
		}
		return "1";
	}

	@GetMapping(value = "/setPhoneBook2")
	public String setPhoneBook2(@RequestParam("devId") String devId, HttpServletRequest request) {
		try {
			System.out.println("设备id：" + devId);

			String devId2;
			if (devId.startsWith("20CH")) {
				devId2 = devId.substring(4);
			} else {
				devId2 = devId;
			}
			System.out.println("去前缀后的devId：" + devId2);
			List<Phone> p = phoneRepo.findByDevId("20CH" + devId2);

			if (p == null || p.isEmpty()) {
				return "2";
			}
			Phone phone = p.get(0);
			System.out.println("phonebook:" + phone.getPhoneBook());

			String[] sArr = phone.getPhoneBook().split(",");
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < sArr.length; i++) {
				if ((i + 1) % 2 == 0) {
					builder.append(stringToUnicode(sArr[i]));
				} else {
					builder.append(sArr[i]);
				}
				if (i < sArr.length - 1) {
					builder.append(",");
				}
			}

			int length = 5 + builder.toString().length();
			String hex = Integer.toHexString(length);
			String newHex;
			if (hex.length() == 1) {
				newHex = "000" + hex;
			} else if (hex.length() == 2) {
				newHex = "00" + hex;

			} else if (hex.length() == 3) {
				newHex = "0" + hex;
			} else {
				newHex = hex;
			}
			String msg = "[3g*" + devId2 + "*" + newHex + "*PHB2," + builder.toString() + "]";
			System.out.println(msg);
			NettyConfig.sendMessage(devId2, msg);

		} catch (Exception e) {
			e.printStackTrace();
			return "3";
		}
		return "1";
	}

	private static String stringToUnicode(String string) {
		StringBuffer unicode = new StringBuffer();
		for (int i = 0; i < string.length(); i++) {
			char c = string.charAt(i); // 取出每一个字符
			unicode.append(Integer.toHexString(c));// 转换为unicode
		}
		return unicode.toString();
	}
}
