package com.wdl.datarest.implementation;

/**
 * 手环心率、高压、低压数据
 * 
 * @author Administrator
 *
 */
public class ShouHuanData {

	private String devId;

	private short heartRate;// 心率
	private short hv;// 高压
	private short lv;// 低压

	private Boolean isSetHeartRate;// 已设置心率数据

	private Boolean isSetHlv;// 已设置高低压数据

	public String getDevId() {
		return devId;
	}

	public void setDevId(String devId) {
		this.devId = devId;
	}

	public short getHeartRate() {
		return heartRate;
	}

	public void setHeartRate(short heartRate) {
		this.heartRate = heartRate;
	}

	public short getHv() {
		return hv;
	}

	public void setHv(short hv) {
		this.hv = hv;
	}

	public short getLv() {
		return lv;
	}

	public void setLv(short lv) {
		this.lv = lv;
	}

	public Boolean getIsSetHeartRate() {
		return isSetHeartRate;
	}

	public void setIsSetHeartRate(Boolean isSetHeartRate) {
		this.isSetHeartRate = isSetHeartRate;
	}

	public Boolean getIsSetHlv() {
		return isSetHlv;
	}

	public void setIsSetHlv(Boolean isSetHlv) {
		this.isSetHlv = isSetHlv;
	}

}
