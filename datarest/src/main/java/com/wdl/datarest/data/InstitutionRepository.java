package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "institution")
public interface InstitutionRepository extends PagingAndSortingRepository<Institution, Long> {
    @RestResource(path="id")
    List<Institution> findById(@Param("id") long id);

    @RestResource(path="name")
    Page<Institution> findByInstNameContainsIgnoringCase(@Param("name") String instName, Pageable p);

    @RestResource(path="instName")
    List<Institution> findByInstName(@Param("instName") String instName);

    @RestResource(path="ip")
    List<Institution> findByIp(@Param("ip") String ip);
}
