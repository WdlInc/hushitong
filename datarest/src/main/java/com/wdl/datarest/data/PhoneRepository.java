package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "phone")
public interface PhoneRepository extends PagingAndSortingRepository<Phone, Long> {
	@RestResource(path = "findByDevId")
	List<Phone> findByDevId(@Param("devId") String devId);
}
