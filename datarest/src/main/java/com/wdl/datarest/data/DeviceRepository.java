package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import io.swagger.annotations.ApiOperation;

@RepositoryRestResource(path = "device")
public interface DeviceRepository extends PagingAndSortingRepository<Device, Long> {
    @RestResource(path="id")
    List<Device> findById(@Param("id") long id);

    @ApiOperation(value="Get device by person ID")
    @RestResource(path="personId")
    List<Device> findByPersonId(@Param("personId") long personId);

    @ApiOperation(value="Get device by deviceId in string")
    @RestResource(path="deviceId")
    List<Device> findByDeviceId(@Param("deviceId") String deviceId);

    @Query(value = "select * from device where id = ?1 limit 1", nativeQuery = true)
    Device findByIndex(long index);

    @RestResource(path="all")
    Page<Device> findByInstitutionId(@Param("institutionId") long institutionId, Pageable p);

    @RestResource(path="deviceIds")
    Page<Device> findByInstitutionIdAndDeviceIdContainsIgnoringCase(@Param("institutionId") long institutionId, @Param("deviceId") String deviceId, Pageable p);

    @RestResource(path="deviceTypeIds")
    Page<Device> findByInstitutionIdAndDeviceIdStartsWithAndDeviceIdContainsIgnoringCase(@Param("institutionId") long institutionId, @Param("deviceType") String deviceType, @Param("deviceId") String deviceId, Pageable p);

    @RestResource(path="allTypes")
    Page<Device> findByInstitutionIdAndDeviceIdStartingWith(@Param("institutionId") long institutionId, @Param("deviceType") String deviceType, Pageable p);

    @ApiOperation(value="Get the total number of devices in an institution")
    @RestResource(path="countall")
    long countByInstitutionId(@Param("institutionId") long institutionId);

    @ApiOperation(value="Get the total number of in-use devices in an institution")
    @RestResource(path="countinuse")
    @Query(value = "select count(*) from device where institution_id = ?1 and person_id != 0", nativeQuery = true)
    long countInUse(@Param("institutionId") long institutionId);

    @ApiOperation(value="Get the total number of abnormal devices in an institution")
    @RestResource(path="countabnormal")
    @Query(value = "select count(*) from device where institution_id = ?1 and person_id != 0 and update_time < ?2", nativeQuery = true)
    long countAbnormal(@Param("institutionId") long institutionId, @Param("updateTime") long updateTime);

    @ApiOperation(value="Get all abnormal devices in an institution")
    @RestResource(path="findabnormal")
    @Query(value = "select * from device where institution_id = ?1 and person_id != 0 and update_time < ?2", nativeQuery = true)
    List<Device> findAbnormal(@Param("institutionId") long institutionId, @Param("updateTime") long updateTime);

    @ApiOperation(value="Get all in-use devices in an institution")
    @RestResource(path="findinuse")
    @Query(value = "select * from device where institution_id = ?1 and person_id != 0", nativeQuery = true)
    List<Device> findInUse(@Param("institutionId") long institutionId);

    @ApiOperation(value="Get all available devices in an institution")
    @RestResource(path="findtouse")
    @Query(value = "select * from device where institution_id = ?1 and person_id = 0", nativeQuery = true)
    List<Device> findToUse(@Param("institutionId") long institutionId);

    @ApiOperation(value="Get all devices in an institution")
    @RestResource(path="findall")
    @Query(value = "select * from device where institution_id = ?1", nativeQuery = true)
    List<Device> findAll(@Param("institutionId") long institutionId);
}
