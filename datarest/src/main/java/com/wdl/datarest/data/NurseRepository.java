package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import io.swagger.annotations.ApiOperation;

@RepositoryRestResource(path = "nurse")
public interface NurseRepository extends PagingAndSortingRepository<Nurse, Long> {
    @RestResource(path="id")
    List<Nurse> findById(@Param("id") long id);

    @RestResource(path="all")
    Page<Nurse> findByInstitutionId(@Param("institutionId") long institutionId, Pageable p);

    @ApiOperation(value="Get all nurses in an institution")
    @RestResource(path="findall")
    @Query(value = "select * from nurse where institution_id = ?1", nativeQuery = true)
    List<Nurse> findAll(@Param("institutionId") long institutionId);

    @RestResource(path="name")
    Page<Nurse> findByInstitutionIdAndNurseNameContainsIgnoringCase(@Param("institutionId") long institutionId, @Param("name") String nurseName, Pageable p);

    @RestResource(path="departmentName")
    List<Nurse> findByInstitutionIdAndDepartmentName(@Param("institutionId") long institutionId, @Param("departmentName") String departmentName);

    @RestResource(path="nurseName")
    List<Nurse> findByInstitutionIdAndNurseName(@Param("institutionId") long institutionId, @Param("nurseName") String nurseName);
}