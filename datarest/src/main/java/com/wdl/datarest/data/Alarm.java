package com.wdl.datarest.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Alarm {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column
    long institutionId;

    @Size(max = 16)
    @Column
    String deviceId;

    @Column
    long personId;

    @Size(max = 40)
    @Column
    String personName;

    @NotNull
    @Column
    AlarmType type;

    @Column
    short value;

    @Column
    long raisedTime;

    @Column
    long cleardTime;

    @Column
    long ackedTime;

    @Column
    int spare1;

    @Column
    long spare2;

    @Size(max = 16)
    @Column
    String spare3;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getInstitutionId() {
        return institutionId;
    }

    public void setInstitutionId(long institutionId) {
        this.institutionId = institutionId;
    }

    public AlarmType getType() {
        return type;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public void setType(AlarmType type) {
        this.type = type;
    }

    public short getValue() {
        return value;
    }

    public void setValue(short value) {
        this.value = value;
    }

    public long getRaisedTime() {
        return raisedTime;
    }

    public void setRaisedTime(long raisedTime) {
        this.raisedTime = raisedTime;
    }

    public long getCleardTime() {
        return cleardTime;
    }

    public void setCleardTime(long cleardTime) {
        this.cleardTime = cleardTime;
    }

    public long getAckedTime() {
        return ackedTime;
    }

    public void setAckedTime(long ackedTime) {
        this.ackedTime = ackedTime;
    }

    public int getSpare1() {
        return spare1;
    }

    public void setSpare1(int spare1) {
        this.spare1 = spare1;
    }

    public long getSpare2() {
        return spare2;
    }

    public void setSpare2(long spare2) {
        this.spare2 = spare2;
    }

    public String getSpare3() {
        return spare3;
    }

    public void setSpare3(String spare3) {
        this.spare3 = spare3;
    }
}