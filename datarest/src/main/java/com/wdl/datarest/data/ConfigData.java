package com.wdl.datarest.data;

public class ConfigData {
    long personId;
    String devId;

    // type could be checkin, checkout, delete, updateDevice, updateThreshold, updateBasic, updateContact, updateHospital
    String type;

    public long getPersonId() {
        return personId;
    }

    public void setPersonId(long personId) {
        this.personId = personId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDevId() {
        return devId;
    }

    public void setDevId(String devId) {
        this.devId = devId;
    }
}
