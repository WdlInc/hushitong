package com.wdl.datarest.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Size;

@Entity
public class Phone {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(updatable = true, nullable = false, unique = true)
	@Size(max = 16)
	private String devId;
	@Column
	@Size(max = 2000)
	private String phoneBook;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDevId() {
		return devId;
	}

	public void setDevId(String devId) {
		this.devId = devId;
	}

	public String getPhoneBook() {
		return phoneBook;
	}

	public void setPhoneBook(String phoneBook) {
		this.phoneBook = phoneBook;
	}

}
