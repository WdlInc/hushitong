package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import io.swagger.annotations.ApiOperation;

@RepositoryRestResource(path = "bed")
public interface BedRepository extends PagingAndSortingRepository<Bed, Long> {
    @RestResource(path="id")
    List<Bed> findById(@Param("id") long id);

    @ApiOperation(value="Get the bed info by a person ID")
    @RestResource(path="personId")
    List<Bed> findByPersonId(@Param("personId") long personId);
}