package com.wdl.datarest.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(max = 40)
    @Column
    String personName;

    @NotNull
    @Size(max = 20)
    @Column
    String rid;

    @NotNull
    @Column
    Sex sex;

    @Column
    long birth;

    @Column
    @Size(max = 20)
    String nation;

    @Size(max = 12)
    @Column
    String phone;

    @Size(max = 128)
    @Column
    String address;

    @Column
    Education education;

    @Column
    @Size(max = 40)
    String contact1Name;

    @Column
    Sex contact1Sex;

    @Size(max = 12)
    @Column
    String contact1Phone;

    @Size(max = 128)
    @Column
    String contact1Address;

    @Column
    @Size(max = 40)
    String contact2Name;

    @Column
    Sex contact2Sex;

    @Size(max = 12)
    @Column
    String contact2Phone;

    @Size(max = 128)
    @Column
    String contact2Address;

    // Flags to indicate the current alarm states
    @Column
    boolean heartAlarmFlag;

    @Column
    long heartAlarmId;

    @Column
    boolean breatheAlarmFlag;

    @Column
    long breatheAlarmId;

    @Column
    boolean upAlarmFlag;

    @Column
    long upAlarmId;

    @Column
    boolean sideAlarmFlag;

    @Column
    long sideAlarmId;

    @Column
    boolean awayAlarmFlag;

    @Column
    long awayAlarmId;

    @Column
    boolean wetAlarmFlag;

    @Column
    long wetAlarmId;

    @Column
    boolean moveAlarmFlag;

    @Column
    long moveAlarmId;

    @Column
    boolean ringAlarmFlag;

    @Column
    long ringAlarmId;

    @Column
    boolean deviceAlarmFlag;

    @Column
    long deviceAlarmId;

    @Column
    boolean leftAlarmFlag;

    @Column
    long leftAlarmId;

    @Column
    boolean rightAlarmFlag;

    @Column
    long rightAlarmId;

    @Column(columnDefinition = "bit default 0")
    boolean turnOverReminderAlarmFlag;

    @Column(columnDefinition = "bigint default 0")
    long turnOverReminderAlarmId;

    @Column(columnDefinition = "smallint default 0")
    short turnOverReminder;

    // Flags to indicate if to hide the alarm on GUI, like acked alarms
    @Column
    boolean hideHeartAlarm;

    @Column
    boolean hideBreatheAlarm;

    @Column
    boolean hideUpAlarm;

    @Column
    boolean hideSideAlarm;

    @Column
    boolean hideAwayAlarm;

    @Column
    boolean hideWetAlarm;

    @Column
    boolean hideMoveAlarm;

    @Column
    boolean hideRingAlarm;

    @Column
    boolean hideDeviceAlarm;

    @Column
    boolean hideLeftAlarm;

    @Column
    boolean hideRightAlarm;

    @Column
    short heartUp;

    @Column
    short heartLow;

    @Column
    short breatheUp;

    @Column
    short breatheLow;

    @Column
    short move;

    // Indicates if need to fire alarms on device side
    @Column
    boolean heartAlarm;

    @Column
    boolean breatheAlarm;

    @Column
    boolean upAlarm;

    @Column
    boolean sideAlarm;

    @Column
    boolean silent;

    @Column
    boolean awayAlarm;

    @Column
    boolean moveAlarm;

    @Column
    long moveAlarmAckTime;

    @Column
    boolean deviceAlarm;

    @Column
    long checkinTime;

    @Column
    long checkoutTime;

    @Column
    @Size(max = 256)
    String checkinRecord;

    @Column
    @Size(max = 256)
    String checkoutRecord;

    @Column
    long bedId;

    @Column
    @Size(max = 64)
    String bedString;

    @Column
    long nurseId;

    @Column
    @Size(max = 64)
    String nurseName;

    @Column
    @Size(max = 20)
    String device1Id;

    @Column
    long device1;

    @Column
    long bind1Time;

    @Column
    long unbind1Time;

    @Column
    @Size(max = 20)
    String device2Id;

    @Column
    long device2;

    @Column
    long bind2Time;

    @Column
    long unbind2Time;

    @Column
    @Size(max = 20)
    String device3Id;

    @Column
    long device3;

    @Column
    long bind3Time;

    @Column
    long unbind3Time;

    @Column
    @Size(max = 20)
    String device4Id;

    @Column
    long device4;

    @Column
    long bind4Time;

    @Column
    long unbind4Time;

    @ManyToOne
    @JoinColumn(name = "institution_id")
    private Institution institution;

    @Column(columnDefinition = "double default 0")
    Double longitude;

    @Column(columnDefinition = "double default 0")
    Double latitude;

    @Column(columnDefinition = "double default 0")
    Double kilo;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public double getKilo() {
        return kilo;
    }

    public void setKilo(Double kilo) {
        this.kilo = kilo;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getRid() {
        return rid;
    }

    public void setRid(String rid) {
        this.rid = rid;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public long getBirth() {
        return birth;
    }

    public void setBirth(long birth) {
        this.birth = birth;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    public String getContact1Name() {
        return contact1Name;
    }

    public void setContact1Name(String contact1Name) {
        this.contact1Name = contact1Name;
    }

    public Sex getContact1Sex() {
        return contact1Sex;
    }

    public void setContact1Sex(Sex contact1Sex) {
        this.contact1Sex = contact1Sex;
    }

    public String getContact1Phone() {
        return contact1Phone;
    }

    public void setContact1Phone(String contact1Phone) {
        this.contact1Phone = contact1Phone;
    }

    public String getContact1Address() {
        return contact1Address;
    }

    public void setContact1Address(String contact1Address) {
        this.contact1Address = contact1Address;
    }

    public String getContact2Name() {
        return contact2Name;
    }

    public void setContact2Name(String contact2Name) {
        this.contact2Name = contact2Name;
    }

    public Sex getContact2Sex() {
        return contact2Sex;
    }

    public void setContact2Sex(Sex contact2Sex) {
        this.contact2Sex = contact2Sex;
    }

    public String getContact2Phone() {
        return contact2Phone;
    }

    public void setContact2Phone(String contact2Phone) {
        this.contact2Phone = contact2Phone;
    }

    public String getContact2Address() {
        return contact2Address;
    }

    public void setContact2Address(String contact2Address) {
        this.contact2Address = contact2Address;
    }

    public short getHeartUp() {
        return heartUp;
    }

    public void setHeartUp(short heartUp) {
        this.heartUp = heartUp;
    }

    public short getHeartLow() {
        return heartLow;
    }

    public void setHeartLow(short heartLow) {
        this.heartLow = heartLow;
    }

    public short getBreatheUp() {
        return breatheUp;
    }

    public void setBreatheUp(short breatheUp) {
        this.breatheUp = breatheUp;
    }

    public short getBreatheLow() {
        return breatheLow;
    }

    public void setBreatheLow(short breatheLow) {
        this.breatheLow = breatheLow;
    }

    public short getMove() {
        return move;
    }

    public void setMove(short move) {
        this.move = move;
    }

    public boolean isUpAlarm() {
        return upAlarm;
    }

    public void setUpAlarm(boolean upAlarm) {
        this.upAlarm = upAlarm;
    }

    public boolean isAwayAlarm() {
        return awayAlarm;
    }

    public void setAwayAlarm(boolean awayAlarm) {
        this.awayAlarm = awayAlarm;
    }

    public boolean isSideAlarm() {
        return sideAlarm;
    }

    public void setSideAlarm(boolean sideAlarm) {
        this.sideAlarm = sideAlarm;
    }

    public boolean isMoveAlarm() {
        return moveAlarm;
    }

    public void setMoveAlarm(boolean moveAlarm) {
        this.moveAlarm = moveAlarm;
    }

    public boolean isDeviceAlarm() {
        return deviceAlarm;
    }

    public void setDeviceAlarm(boolean deviceAlarm) {
        this.deviceAlarm = deviceAlarm;
    }

    public void setMoveAlarmAckTime(long moveAlarmAckTime) {
        this.moveAlarmAckTime = moveAlarmAckTime;
    }

    public long getMoveAlarmAckTime() {
        return moveAlarmAckTime;
    }

    public boolean isSilent() {
        return silent;
    }

    public void setSilent(boolean silent) {
        this.silent = silent;
    }

    public long getCheckinTime() {
        return checkinTime;
    }

    public void setCheckinTime(long checkinTime) {
        this.checkinTime = checkinTime;
    }

    public long getCheckoutTime() {
        return checkoutTime;
    }

    public void setCheckoutTime(long checkoutTime) {
        this.checkoutTime = checkoutTime;
    }

    public String getCheckinRecord() {
        return checkinRecord;
    }

    public void setCheckinRecord(String checkinRecord) {
        this.checkinRecord = checkinRecord;
    }

    public String getCheckoutRecord() {
        return checkoutRecord;
    }

    public void setCheckoutRecord(String checkoutRecord) {
        this.checkoutRecord = checkoutRecord;
    }

    public long getBedId() {
        return bedId;
    }

    public void setBedId(long bedId) {
        this.bedId = bedId;
    }

    public long getNurseId() {
        return nurseId;
    }

    public void setNurseId(long nurseId) {
        this.nurseId = nurseId;
    }

    public String getBedString() {
        return bedString;
    }

    public void setBedString(String bedString) {
        this.bedString = bedString;
    }

    public String getNurseName() {
        return nurseName;
    }

    public void setNurseName(String nurseName) {
        this.nurseName = nurseName;
    }

    public String getDevice1Id() {
        return device1Id;
    }

    public void setDevice1Id(String device1Id) {
        this.device1Id = device1Id;
    }

    public long getBind1Time() {
        return bind1Time;
    }

    public void setBind1Time(long bind1Time) {
        this.bind1Time = bind1Time;
    }

    public long getUnbind1Time() {
        return unbind1Time;
    }

    public void setUnbind1Time(long unbind1Time) {
        this.unbind1Time = unbind1Time;
    }

    public long getDevice1() {
        return device1;
    }

    public void setDevice1(long device1) {
        this.device1 = device1;
    }

    public long getDevice2() {
        return device2;
    }

    public void setDevice2(long device2) {
        this.device2 = device2;
    }

    public String getDevice2Id() {
        return device2Id;
    }

    public void setDevice2Id(String device2Id) {
        this.device2Id = device2Id;
    }

    public long getBind2Time() {
        return bind2Time;
    }

    public void setBind2Time(long bind2Time) {
        this.bind2Time = bind2Time;
    }

    public long getUnbind2Time() {
        return unbind2Time;
    }

    public void setUnbind2Time(long unbind2Time) {
        this.unbind2Time = unbind2Time;
    }

    public long getBind3Time() {
        return bind3Time;
    }

    public void setBind3Time(long bind3Time) {
        this.bind3Time = bind3Time;
    }

    public long getUnbind3Time() {
        return unbind3Time;
    }

    public void setUnbind3Time(long unbind3Time) {
        this.unbind3Time = unbind3Time;
    }

    public String getDevice3Id() {
        return device3Id;
    }

    public void setDevice3Id(String device3Id) {
        this.device3Id = device3Id;
    }

    public long getDevice3() {
        return device3;
    }

    public void setDevice3(long device3) {
        this.device3 = device3;
    }

    public String getDevice4Id() {
        return device4Id;
    }

    public void setDevice4Id(String device4Id) {
        this.device4Id = device4Id;
    }

    public long getDevice4() {
        return device4;
    }

    public void setDevice4(long device4) {
        this.device4 = device4;
    }

    public long getBind4Time() {
        return bind4Time;
    }

    public void setBind4Time(long bind4Time) {
        this.bind4Time = bind4Time;
    }

    public long getUnbind4Time() {
        return unbind4Time;
    }

    public void setUnbind4Time(long unbind4Time) {
        this.unbind4Time = unbind4Time;
    }

    public Institution getInstitution() {
        return institution;
    }

    public void setInstitution(Institution institution) {
        this.institution = institution;
    }

    public boolean isHeartAlarmFlag() {
        return heartAlarmFlag;
    }

    public void setHeartAlarmFlag(boolean heartAlarmFlag) {
        this.heartAlarmFlag = heartAlarmFlag;
    }

    public boolean isBreatheAlarmFlag() {
        return breatheAlarmFlag;
    }

    public void setBreatheAlarmFlag(boolean breatheAlarmFlag) {
        this.breatheAlarmFlag = breatheAlarmFlag;
    }

    public boolean isUpAlarmFlag() {
        return upAlarmFlag;
    }

    public void setUpAlarmFlag(boolean upAlarmFlag) {
        this.upAlarmFlag = upAlarmFlag;
    }

    public boolean isSideAlarmFlag() {
        return sideAlarmFlag;
    }

    public void setSideAlarmFlag(boolean sideAlarmFlag) {
        this.sideAlarmFlag = sideAlarmFlag;
    }

    public boolean isAwayAlarmFlag() {
        return awayAlarmFlag;
    }

    public void setAwayAlarmFlag(boolean awayAlarmFlag) {
        this.awayAlarmFlag = awayAlarmFlag;
    }

    public boolean isWetAlarmFlag() {
        return wetAlarmFlag;
    }

    public void setWetAlarmFlag(boolean wetAlarmFlag) {
        this.wetAlarmFlag = wetAlarmFlag;
    }

    public boolean isMoveAlarmFlag() {
        return moveAlarmFlag;
    }

    public void setMoveAlarmFlag(boolean moveAlarmFlag) {
        this.moveAlarmFlag = moveAlarmFlag;
    }

    public boolean isRingAlarmFlag() {
        return ringAlarmFlag;
    }

    public void setRingAlarmFlag(boolean ringAlarmFlag) {
        this.ringAlarmFlag = ringAlarmFlag;
    }

    public boolean isDeviceAlarmFlag() {
        return deviceAlarmFlag;
    }

    public void setDeviceAlarmFlag(boolean deviceAlarmFlag) {
        this.deviceAlarmFlag = deviceAlarmFlag;
    }

    public boolean isLeftAlarmFlag() {
        return leftAlarmFlag;
    }

    public void setLeftAlarmFlag(boolean leftAlarmFlag) {
        this.leftAlarmFlag = leftAlarmFlag;
    }

    public boolean isRightAlarmFlag() {
        return rightAlarmFlag;
    }

    public void setRightAlarmFlag(boolean rightAlarmFlag) {
        this.rightAlarmFlag = rightAlarmFlag;
    }

    public boolean isHeartAlarm() {
        return heartAlarm;
    }

    public void setHeartAlarm(boolean heartAlarm) {
        this.heartAlarm = heartAlarm;
    }

    public boolean isBreatheAlarm() {
        return breatheAlarm;
    }

    public void setBreatheAlarm(boolean breatheAlarm) {
        this.breatheAlarm = breatheAlarm;
    }

    public boolean isHideHeartAlarm() {
        return hideHeartAlarm;
    }

    public void setHideHeartAlarm(boolean hideHeartAlarm) {
        this.hideHeartAlarm = hideHeartAlarm;
    }

    public boolean isHideBreatheAlarm() {
        return hideBreatheAlarm;
    }

    public void setHideBreatheAlarm(boolean hideBreatheAlarm) {
        this.hideBreatheAlarm = hideBreatheAlarm;
    }

    public boolean isHideUpAlarm() {
        return hideUpAlarm;
    }

    public void setHideUpAlarm(boolean hideUpAlarm) {
        this.hideUpAlarm = hideUpAlarm;
    }

    public boolean isHideSideAlarm() {
        return hideSideAlarm;
    }

    public void setHideSideAlarm(boolean hideSideAlarm) {
        this.hideSideAlarm = hideSideAlarm;
    }

    public boolean isHideAwayAlarm() {
        return hideAwayAlarm;
    }

    public void setHideAwayAlarm(boolean hideAwayAlarm) {
        this.hideAwayAlarm = hideAwayAlarm;
    }

    public boolean isHideWetAlarm() {
        return hideWetAlarm;
    }

    public void setHideWetAlarm(boolean hideWetAlarm) {
        this.hideWetAlarm = hideWetAlarm;
    }

    public boolean isHideDeviceAlarm() {
        return hideDeviceAlarm;
    }

    public void setHideDeviceAlarm(boolean hideDeviceAlarm) {
        this.hideDeviceAlarm = hideDeviceAlarm;
    }

    public boolean isHideMoveAlarm() {
        return hideMoveAlarm;
    }

    public void setHideMoveAlarm(boolean hideMoveAlarm) {
        this.hideMoveAlarm = hideMoveAlarm;
    }

    public boolean isHideRingAlarm() {
        return hideRingAlarm;
    }

    public void setHideRingAlarm(boolean hideRingAlarm) {
        this.hideRingAlarm = hideRingAlarm;
    }

    public boolean isHideLeftAlarm() {
        return hideLeftAlarm;
    }

    public void setHideLeftAlarm(boolean hideLeftAlarm) {
        this.hideLeftAlarm = hideLeftAlarm;
    }

    public boolean isHideRightAlarm() {
        return hideRightAlarm;
    }

    public void setHideRightAlarm(boolean hideRightAlarm) {
        this.hideRightAlarm = hideRightAlarm;
    }

    public long getHeartAlarmId() {
        return heartAlarmId;
    }

    public void setHeartAlarmId(long heartAlarmId) {
        this.heartAlarmId = heartAlarmId;
    }

    public long getBreatheAlarmId() {
        return breatheAlarmId;
    }

    public void setBreatheAlarmId(long breatheAlarmId) {
        this.breatheAlarmId = breatheAlarmId;
    }

    public long getUpAlarmId() {
        return upAlarmId;
    }

    public void setUpAlarmId(long upAlarmId) {
        this.upAlarmId = upAlarmId;
    }

    public long getSideAlarmId() {
        return sideAlarmId;
    }

    public void setSideAlarmId(long sideAlarmId) {
        this.sideAlarmId = sideAlarmId;
    }

    public long getAwayAlarmId() {
        return awayAlarmId;
    }

    public void setAwayAlarmId(long awayAlarmId) {
        this.awayAlarmId = awayAlarmId;
    }

    public long getWetAlarmId() {
        return wetAlarmId;
    }

    public void setWetAlarmId(long wetAlarmId) {
        this.wetAlarmId = wetAlarmId;
    }

    public long getMoveAlarmId() {
        return moveAlarmId;
    }

    public void setMoveAlarmId(long moveAlarmId) {
        this.moveAlarmId = moveAlarmId;
    }

    public long getRingAlarmId() {
        return ringAlarmId;
    }

    public void setRingAlarmId(long ringAlarmId) {
        this.ringAlarmId = ringAlarmId;
    }

    public long getDeviceAlarmId() {
        return deviceAlarmId;
    }

    public void setDeviceAlarmId(long deviceAlarmId) {
        this.deviceAlarmId = deviceAlarmId;
    }

    public long getLeftAlarmId() {
        return leftAlarmId;
    }

    public void setLeftAlarmId(long leftAlarmId) {
        this.leftAlarmId = leftAlarmId;
    }

    public long getRightAlarmId() {
        return rightAlarmId;
    }

    public void setRightAlarmId(long rightAlarmId) {
        this.rightAlarmId = rightAlarmId;
    }

    public boolean isTurnOverReminderAlarmFlag() {
        return turnOverReminderAlarmFlag;
    }

    public void setTurnOverReminderAlarmFlag(boolean turnOverReminderAlarmFlag) {
        this.turnOverReminderAlarmFlag = turnOverReminderAlarmFlag;
    }

    public long getTurnOverReminderAlarmId() {
        return turnOverReminderAlarmId;
    }

    public void setTurnOverReminderAlarmId(long turnOverReminderAlarmId) {
        this.turnOverReminderAlarmId = turnOverReminderAlarmId;
    }

    public short getTurnOverReminder() {
        return turnOverReminder;
    }

    public void setTurnOverReminder(short turnOverReminder) {
        this.turnOverReminder = turnOverReminder;
    }
}

enum Sex {
    MALE, FEMALE
}

enum Education {
    NONE, PRIMARY, MIDDLE, COLLEGE
}
