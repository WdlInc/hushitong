package com.wdl.datarest.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProRangeResponse {
    private String status;
    private ProRangeData data;

    public boolean isSuccess() {
        return "success".equals(this.status);
    }

    public Object[] getCounters() {
        if (this.getData().getResult().length == 0) {
            return null;
        }

        return this.getData().getResult()[0].getValues();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProRangeData getData() {
        return data;
    }

    public void setData(ProRangeData data) {
        this.data = data;
    }
}