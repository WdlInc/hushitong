package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

@RepositoryRestResource(path = "level")
public interface LevelRepository extends PagingAndSortingRepository<Level, Long> {
    @RestResource(path="id")
    List<Level> findById(@Param("Id") long id);
}