package com.wdl.datarest.data;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import io.swagger.annotations.ApiOperation;

@RepositoryRestResource(path = "department")
public interface DepartmentRepository extends PagingAndSortingRepository<Department, Long> {
    @RestResource(path="id")
    List<Department> findById(@Param("id") long id);

    @RestResource(path="all")
    Page<Department> findByInstitutionId(@Param("institutionId") long institutionId, Pageable p);

    @ApiOperation(value="Get all departments in an institution")
    @RestResource(path="allList")
    List<Department> findByInstitutionId(@Param("institutionId") long institutionId);

    @RestResource(path="name")
    Page<Department> findByInstitutionIdAndDepartmentNameContainsIgnoringCase(@Param("institutionId") long institutionId, @Param("name") String departmentName, Pageable p);
}
