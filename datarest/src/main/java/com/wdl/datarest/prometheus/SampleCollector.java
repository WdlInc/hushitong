package com.wdl.datarest.prometheus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.wdl.datarest.implementation.LogUtils;

@Component
public class SampleCollector {
    @SuppressWarnings("unused")
    private static Logger log = LoggerFactory.getLogger("SampleCollector");

    @Value("${pro.debug.enabled:false}")
    boolean debugging;

    @Value("${pro.debug.alarm:move}")
    String alarmString;

    @Value("${pro.debug.interval:5000}")
    long interval;

    @Value("${pro.debug.person:1}")
    long person;

    @Autowired
    HealthData healthData;

    Random heartValue = new Random(100);
    Random breatheValue = new Random(20);
    Random sleepValue = new Random(1);

    @PostConstruct
    public void initialize() {
        if (!this.debugging) {
            return;
        }

        List<String> alarms = new ArrayList<String>();
        if (!StringUtils.isEmpty(alarmString)) {
            for (String item : alarmString.split(",")) {
                if (!StringUtils.isEmpty(item.trim())) {
                    alarms.add(item.trim());
                }
            }
        }

        Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() -> {
            try {
                // For hist alarms to elasticsearch
                long currentTime = new Date().getTime();
                LogUtils.logBreatheHistData(person, currentTime, this.breatheValue.nextInt(20));
                LogUtils.logHeartHistData(person, currentTime, this.heartValue.nextInt(100));
                LogUtils.logMoveHistPerHour(person, currentTime, this.breatheValue.nextInt(10));

                /*
                LogUtils.logBreatheAlarm(person, currentTime, currentTime + this.breatheValue.nextInt(10) * 1000, this.breatheValue.nextInt(20) + 10);
                LogUtils.logHeartAlarm(person, currentTime, currentTime + this.breatheValue.nextInt(20) * 1000, this.heartValue.nextInt(20) + 80);

                LogUtils.logAlarm(person, AlarmType.UP, currentTime, currentTime + this.breatheValue.nextInt(20) * 1000);
                LogUtils.logAlarm(person, AlarmType.SIDE, currentTime, currentTime + this.breatheValue.nextInt(10) * 1000);
                LogUtils.logAlarm(person, AlarmType.AWAY, currentTime, currentTime + this.breatheValue.nextInt(20) * 1000);
                LogUtils.logAlarm(person, AlarmType.WET, currentTime, currentTime + this.breatheValue.nextInt(10) * 1000);

                LogUtils.logLeftAlarm(person, currentTime);
                LogUtils.logRightAlarm(person, currentTime);
                */

                // For active data to prometheus
                /*
                int heartV = this.heartValue.nextInt(100);
                int breatheV = this.breatheValue.nextInt(20);
                int sleepV = this.sleepValue.nextInt(1);
                this.healthData.processHeartBreatheData(person, heartV, breatheV);

                for (String alarm : alarms) {
                    switch (alarm) {
                    case "move":
                        this.healthData.incBodyMoveCount(person);
                        break;
                    case "away":
                        this.healthData.incAwayAlarmCount(person);
                        break;
                    case "breathe":
                        this.healthData.incBreatheAlarmCount(person);
                        break;
                    case "heart":
                        this.healthData.incHeartAlarmCount(person);
                        break;
                    default:
                        break;
                    }
                }
                */
            } catch (Exception e) {
            }
        }, interval, interval, TimeUnit.MILLISECONDS);
    }
}