package com.wdl.datarest.prometheus;

import org.springframework.stereotype.Component;

import io.prometheus.client.Gauge;

import com.wdl.datarest.data.SleepMode;
import com.wdl.datarest.implementation.CacheData;

@Component
public class HealthData {
    static final Gauge heart = Gauge.build().name("heart").help("heart beat frequency").labelNames("personId").register();
    static final Gauge breathe = Gauge.build().name("breathe").help("breathe frequency").labelNames("personId").register();
    static final Gauge sleep = Gauge.build().name("sleep").help("sleep mode: 0 - AWAKE, 1 - LIGHT, 2 - HEAVY").labelNames("personId").register();

    public void processHeartBreatheData(CacheData cache, int heartValue, int breatheValue) {
        cache.increaseHeart(heartValue);
        cache.inscreaseBreathe(breatheValue);

        String personId = String.valueOf(cache.getPersonId());
        heart.labels(personId.toString()).set(heartValue);
        breathe.labels(personId.toString()).set(breatheValue);
    }

    public void processSleepData(CacheData cache, SleepMode mode) {
        switch (cache.getSleepMode()) {
        case HEAVY:
            cache.increaseDeepSleep();
            break;
        case LIGHT:
            cache.increaseShallowSleep();
            break;
        case AWAKE:
            break;
        }

        cache.setSleepMode(mode);
        cache.setSleepModeStartingTime(System.currentTimeMillis());

        String personId = String.valueOf(cache.getPersonId());
        sleep.labels(personId.toString()).set(mode.ordinal());
    }

    /**
     * Clean up the cache for prometheus to retieve in the future after person is removed.
     */
    public void removeProMetrics4Person(long person_id) {
        String personId = String.valueOf(person_id);
        heart.remove(personId);
        breathe.remove(personId);
        sleep.remove(personId);
    }
}