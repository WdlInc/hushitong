package com.wdl.datarest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import io.prometheus.client.spring.boot.EnablePrometheusEndpoint;
import springfox.documentation.spring.data.rest.configuration.SpringDataRestConfiguration;

@SpringBootApplication
@EnablePrometheusEndpoint
@Import(SpringDataRestConfiguration.class)
public class DatarestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatarestApplication.class, args);
    }
}
