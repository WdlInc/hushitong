import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { LoginPageModule } from '../pages/login/login.module';
import { NgxEchartsModule } from 'ngx-echarts';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { DevicePage } from '../pages/device/device';
import { ReportPage } from '../pages/report/report';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { WifiPage } from '../pages/wifi/wifi';
import { AlarmPage } from '../pages/alarm/alarm';
import { HelpPage } from '../pages/help/help';
import { HelpSuggestPage } from '../pages/helpsuggest/helpsuggest';
import { UsPage } from '../pages/us/us';
import { LanguagePage } from '../pages/language/language';
import { ValidatePage } from '../pages/validate/validate';
import { ScanPage } from '../pages/scan/scan';
import { TabsPage } from '../pages/tabs/tabs';
import { TabshelpPage } from '../pages/tabshelp/tabshelp';
import { MapPage } from  '../pages/map/map';
import { TelbookPage} from "../pages/telbook/telbook";
import { PhoneeditPage} from "../pages/phoneedit/phoneedit";

import { ActionSheet } from '@ionic-native/action-sheet';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NetworkInterface } from '@ionic-native/network-interface';

import { RestService } from './services/rest.service';
import { UdpService } from './services/udp.service';

import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { PreConfigPage } from '../pages/preconfig/preconfig';
import { AlarmRecPage } from '../pages/alarmRec/alarmRec';
import { UserItemPage } from '../pages/userItem/userItem';

export function createTranslateHttpLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    ReportPage,
    DevicePage,
    ContactPage,
    MapPage,
    HomePage,
    WifiPage,
    AlarmPage,
    HelpPage,
    HelpSuggestPage,
    UsPage,
    LanguagePage,
    ValidatePage,
    ScanPage,
    PreConfigPage,
    AlarmRecPage,
    UserItemPage,
    TabsPage,
    TabshelpPage,
    TelbookPage,
    PhoneeditPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LoginPageModule,
    NgxEchartsModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      mode: 'ios',
      tabsHideOnSubPages: true
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateHttpLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ReportPage,
    DevicePage,
    ContactPage,
    HomePage,
    MapPage,
    WifiPage,
    AlarmPage,
    HelpPage,
    HelpSuggestPage,
    UsPage,
    LanguagePage,
    ValidatePage,
    ScanPage,
    PreConfigPage,
    AlarmRecPage,
    UserItemPage,
    TabsPage,
    TabshelpPage,
    TelbookPage,
    PhoneeditPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RestService,
    UdpService,
    NetworkInterface,
    BarcodeScanner,
    Camera,
    File,
    ActionSheet,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
