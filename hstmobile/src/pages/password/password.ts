import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../app/services/rest.service';
import { NavController, NavParams, ToastController } from 'ionic-angular';

import * as _ from 'lodash';

@Component({
  selector: 'page-password',
  templateUrl: 'password.html',
})
export class PasswordPage {
  group: FormGroup;

  timer = 120;
  timerTick: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private builder: FormBuilder,
              private toastCtrl: ToastController,
              private translate: TranslateService,
              private rest: RestService) {
    this.group = this.builder.group({
      username: [ '', [ Validators.required, Validators.minLength(11), Validators.maxLength(16), Validators.pattern('[\+0-9]+') ] ],
      vcode: [ '', [ Validators.required, Validators.minLength(4), Validators.maxLength(4), Validators.pattern('[0-9]+') ] ],
      password: [ '', [ Validators.required ] ]
    });
  }

  clearUsername() {
    this.group.controls.username.setValue('');
  }

  changePassword() {
    this.rest.post('/api/keycloak/forget-password', {
      phoneNumber: this.group.controls.username.value,
      code: this.group.controls.vcode.value,
      password: this.group.controls.password.value
    }).subscribe(resp => {
      if (resp.status >= 200 && resp.status < 300) {
        this.rest.saveUser({
          username: this.group.controls.username.value,
          password: this.group.controls.password.value,
          locale: '',
          role: ''
        });
        this.navCtrl.pop();
      } else {
        if (resp.status === 404) {
          this.showToast(this.translate.get('userNotFound')['value']);
        } else if (resp.status === 406) {
          this.showToast(this.translate.get('codeMismatch')['value']);
        } else {
          this.showToast(this.translate.get('badRequest')['value']);
        }
      }
    }, error => {
      this.showToast(this.translate.get('badRequest')['value']);
      console.error('Failed to reset password!');
    });
  }

  showToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });

    toast.present(toast);
  }

  getVerificationCode() {
    if (this.timer !== 120) {
      return;
    }

    this.startTick();

    this.rest.getValidateCode(this.group.controls.username.value).subscribe(resp => {
      if (resp.status !== 200) {
        console.error('Failed to get code: ' + resp.info);
        if (resp.info) {
          this.showToast(resp.info);
        }
        this.stopTick();
      }
    }, error => {
      console.error('Failed to get code' + error);
      this.showToast(this.translate.get('badRequest')['value']);
      this.stopTick();
    });
  }

  getCodeText() {
    if (this.timer === 120) {
      return this.translate.get('getCode')['value'];
    } else {
      return this.timer + this.translate.get('waitCode')['value'];
    }
  }

  stopTick() {
    if (this.timerTick) {
      clearInterval(this.timerTick);
      this.timer = 120;
    } else {
      this.timer = 120;
    }
  }

  startTick() {
    this.timer = 99;

    this.timerTick = setInterval(() => {
      if (this.timer <= 1) {
        this.timer = 120;
        if (this.timerTick) {
          clearInterval(this.timerTick);
        }
      } else {
        this.timer--;
      }
    }, 1000);
  }

  navBack() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    const user = this.rest.getUser();
    if (_.isEmpty(user)) {
      return;
    }

    if (user.username) {
      this.group.controls.username.setValue(user.username);
    }
  }
}
