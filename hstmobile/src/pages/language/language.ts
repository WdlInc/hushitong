import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../app/services/rest.service';

@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})
export class LanguagePage {
  currentLang;

  constructor(private navCtrl: NavController,
              private rest: RestService,
              private translate: TranslateService) {
    this.currentLang = translate.currentLang;
  }

  changeLanguage() {
    this.translate.use(this.currentLang);
    this.rest.saveLanguage(this.currentLang);
    this.navCtrl.pop();
  }

  navBack() {
    this.navCtrl.pop();
  }
}
