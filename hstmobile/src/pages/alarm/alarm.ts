import { Component } from '@angular/core';
import { RestService } from '../../app/services/rest.service';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { ValidatePage } from '../validate/validate';
import * as _ from 'lodash';

@Component({
  selector: 'page-alarm',
  templateUrl: 'alarm.html'
})
export class AlarmPage {
  // healthForm: FormGroup;
  personId = null;
  person: any;
  personBf: any;
  bedAlarm: any;
  readonly = true;
  constructor(private rest: RestService,
    public navCtrl: NavController,
    public navParams: NavParams,
    private translate: TranslateService,
    private toastCtrl: ToastController,
    // fb: FormBuilder,
    public alerCtrl: AlertController) {
    this.person = _.cloneDeep(navParams.data.person);
    this.personBf = _.cloneDeep(navParams.data.person);
    this.bedAlarm = this.person.upAlarm || this.person.sideAlarm || this.person.awayAlarm;
  }

  navBack() {
    this.person = _.cloneDeep(this.personBf);
    // this.navCtrl.pop();
    this.popData()

  }

  popData() {
    let callback = this.navParams.get('callback');
    let data: Object = {
      person: this.personBf,
    };
    callback(data);
    this.navCtrl.pop();
  }

  nav2validate() {
    this.navCtrl.push(ValidatePage, {
      person: this.person,
    });
  }

  doConfirm() {
    let confirm = this.alerCtrl.create({
      title: this.translate.get('modificationConfirm')['value'],
      message: '',
      buttons: [
        {
          text: this.translate.get('no')['value'],
          handler: () => {
            this.person = _.cloneDeep(this.personBf);
            this.bedAlarm = this.person.upAlarm || this.person.sideAlarm || this.person.awayAlarm;
            console.log('Disagree clicked');
          }
        },
        {
          text: this.translate.get('yes')['value'],
          handler: () => {
            this.rest.patch('/data/person/' + this.person.id, {
              awayAlarm: this.person.awayAlarm,
              breatheLow: this.person.breatheLow,
              breatheUp: this.person.breatheUp,
              heartLow: this.person.heartLow,
              heartUp: this.person.heartUp,
              move: this.person.move,
              turnOverReminder: this.person.turnOverReminder,
              sideAlarm: this.person.sideAlarm,
              silent: this.person.silent,
              upAlarm: this.person.upAlarm
            }).subscribe(resp => {
              this.showToast(this.translate.get('modifySuccess')['value']);
              console.log('update updateThreshold for ' + this.person.personName + ' successfully!');
            }, message => {
              console.log('modifyPersonError', message);
              // this.refreshSearch();
            });
            this.personBf = _.cloneDeep(this.person);
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present()
  }

  showToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'middle'
    });

    toast.present(toast);
  }
}
