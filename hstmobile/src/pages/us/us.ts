import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RestService } from '../../app/services/rest.service';

@Component({
  selector: 'page-us',
  templateUrl: 'us.html'
})
export class UsPage {
  version = '1.0.0';

  constructor(private navCtrl: NavController, private rest: RestService) {
    this.version = this.rest.getVersion();
  }

  navBack() {
    this.navCtrl.pop();
  }
}
