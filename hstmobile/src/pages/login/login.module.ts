import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TranslateModule } from '@ngx-translate/core';
import { LoginPage } from './login';
import { SignupPage } from '../signup/signup';
import { PasswordPage } from '../password/password';

@NgModule({
  declarations: [
    LoginPage,
    SignupPage,
    PasswordPage
  ],
  imports: [
    IonicPageModule.forChild(LoginPage),
    TranslateModule.forChild()
  ],
  entryComponents: [
    SignupPage,
    PasswordPage
  ]
})
export class LoginPageModule {}
