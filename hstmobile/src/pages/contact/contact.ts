import { Component } from '@angular/core';
import { NavController, AlertController, NavParams, Nav } from 'ionic-angular';
import { RestService } from '../../app/services/rest.service';
import { LoginPage } from '../login/login';
import { WifiPage } from '../wifi/wifi';
import { LanguagePage } from '../language/language';
import { AlarmPage } from '../alarm/alarm';
import { TabshelpPage } from '../tabshelp/tabshelp';
import { TelbookPage} from "../telbook/telbook";
import { UsPage } from '../us/us';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  refreshInterval = 3000;
  refreshTask: any;
  user: any = {};
  public person: any;
  // id: any;
  // personId = null;
  constructor(private rest: RestService,
    private nav: Nav,
    public navParams: NavParams,
    public translate: TranslateService,
    public alerCtrl: AlertController,
    public navCtrl: NavController) {
    this.person = navParams.data;
  }

  logout() {
    let confirm = this.alerCtrl.create({
      title: this.translate.get('confirmLogout')['value'],
      message: '',
      buttons: [
        {
          text: this.translate.get('no')['value'],
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: this.translate.get('yes')['value'],
          handler: () => {
            this.rest.clearup();
            this.nav.setRoot(LoginPage);
            this.rest.validated = false;
            console.log('Agree clicked');
          }
        },
        {
          text: this.translate.get('cleanLogout')['value'],
          handler: () => {
            this.rest.clearup();
            localStorage.clear();
            this.nav.setRoot(LoginPage);
            this.rest.validated = false;
            this.rest.itemRead = false;
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present()
  }

  nav2wifi() {
    this.navCtrl.push(WifiPage, {});
  }

  nav2tel(){
    this.navCtrl.push(TelbookPage,{person:this.person})
  }
  nav2lang() {
    this.navCtrl.push(LanguagePage, {});
  }

  nav2alarm(person) {
    this.navCtrl.push(AlarmPage, {
      person: person,
      callback: data => {
        console.log(data.info);
        this.person = data.person;
      }
    });
  }

  nav2helpsuggest() {
    this.navCtrl.push(TabshelpPage, {});
  }

  nav2us() {
    this.navCtrl.push(UsPage, {});
  }



  refresh() {
    // this.rest.getWithParams('/data/device/search/deviceId', {
    //   deviceId: "01CH000015"
    // }).subscribe(resp => {
    //   this.personId = resp['_embedded']['devices'][0].personId;
    //   this.rest.getWithParams('/data/person/search/id', {
    //     id: this.personId
    //   }).subscribe(resp => {
    //     this.person = resp['_embedded']['persons'][0];
    //   }, message => {
    //     console.error('Failed to get all persons in: ' + message);
    //   });
    // }, message => {
    //   console.error('Failed to get all persons in: ' + message);
    // });
  }

  ngOnInit() {
    // this.refresh();
    // this.refreshTask = setInterval(() => {
    //   this.refresh();
    // }, this.refreshInterval);
  }

  ngOnDestroy() {
    if (this.refreshTask) {
      clearInterval(this.refreshTask);
    }
  }
}
