import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { RestService } from '../../app/services/rest.service';

// @IonicPage()
@Component({
  selector: 'page-userItem',
  templateUrl: 'userItem.html',
})
export class UserItemPage {

  constructor(public navCtrl: NavController,
    private rest: RestService,
    public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad UserItemPage');
  }

  navBack() {
    this.rest.itemRead = true;
    this.navCtrl.pop();
  }

}
