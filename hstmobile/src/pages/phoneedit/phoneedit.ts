import { Component } from '@angular/core';
import {NavController, NavParams, ToastController,Events} from 'ionic-angular';
import { HttpResponse } from '@angular/common/http';
import { RestService } from '../../app/services/rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-phoneedit',
  templateUrl: 'phoneedit.html'
})
export class PhoneeditPage {
  private win: any = window;
  suggestion = '';
  phone = '';
  user: any;
  isLoading = false;
  finished = false;
  phoneId:any;
  myaction:any;
  allphones:any;

  images: Array<{ src: string }>;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public events: Events,
              private toastCtrl: ToastController,
              public translate: TranslateService,
              private rest: RestService) {
    this.user = navParams.data.data;
    console.log(this.user);
    this.phoneId = navParams.data.phoneId;
    this.myaction = navParams.data.action;
    this.allphones = navParams.data.allphones;
    console.log(this.phoneId+","+this.myaction);
    if(this.myaction<100) {
      console.log(this.allphones);
      let mphone = this.allphones[this.myaction-1];
      console.log(mphone);
      this.suggestion = mphone.name;
      this.phone = mphone.phone;
    }
  }

  navBack() {
    this.navCtrl.pop().then(()=>{
      this.events.publish('reloadNotePage');
    });
  }

  submit() {

    let pphone = "";
    if(this.myaction==100) {
      console.log(this.user.allphones);
      if(this.user.allphones!=undefined) {
        pphone = this.user.allphones + "," + this.phone + "," + this.suggestion;
      }else{
        pphone =  this.phone + "," + this.suggestion;
      }
    }else{
      this.allphones[this.myaction-1].name = this.suggestion;
      this.allphones[this.myaction-1].phone = this.phone;
      for(let i=0;i<this.allphones.length;i++){
        let mmphone = this.allphones[i];
        pphone = pphone+ mmphone.phone +","+mmphone.name+",";
      }
      pphone = pphone.substring(0, pphone.length - 1);
    }
    let myUrl ="/data/phone";
    let mdeviceid = this.user.device2Id;
    if(this.phoneId!=null){
      myUrl ='/data/phone/' + this.phoneId;
      this.rest.patch(myUrl, {
        devId: this.user.device2Id,
        phoneBook: pphone
      }).subscribe(resp => {
        this.showToast("联系人设置成功");
        this.rest.get("/api/phonebook/setPhb?devId=" + mdeviceid).subscribe(resp => {

        });
        this.navCtrl.pop().then(() => {
          this.events.publish('reloadNotePage');
        });
      }, message => {
        console.log('modifyPersonError', message);
        // this.refreshSearch();
      });
    }else {
      this.rest.post(myUrl, {
        devId: this.user.device2Id,
        phoneBook: pphone
      }).subscribe(resp => {
        this.showToast("联系人设置成功");
        this.rest.get("/api/phonebook/setPhb?devId=" + mdeviceid).subscribe(resp => {

        });
        this.navCtrl.pop().then(() => {
          this.events.publish('reloadNotePage');
        });
      }, message => {
        console.log('modifyPersonError', message);
        // this.refreshSearch();
      });
    }
  }

  getMessage() {
    if (this.isLoading) {
      return this.translate.get('isLoading')['value'];
    } else if (this.finished) {
      return this.translate.get('suggestionUploaded')['value'];
    }

    return this.translate.get('submit')['value'];
  }

  showToast(message) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom'
    });

    toast.present(toast);
  }

  deletephoto() {
    this.images.shift();
  }
}
