import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WifiPage } from '../wifi/wifi';


@Component({
  selector: 'page-preconfig',
  templateUrl: 'preconfig.html',
})
export class PreConfigPage {
  constructor(public navCtrl: NavController) {
  }

  navBack() {
    this.navCtrl.pop();
  }

  nav2wifi() {
    this.navCtrl.push(WifiPage, {});
  }

}
