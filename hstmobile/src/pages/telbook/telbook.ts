import { Component } from '@angular/core';
import { NavController, NavParams,Events } from 'ionic-angular';
import { ActionSheet, ActionSheetOptions } from '@ionic-native/action-sheet';
import { RestService } from '../../app/services/rest.service';
import { PhoneeditPage} from "../phoneedit/phoneedit";

import { TranslateService } from '@ngx-translate/core';
let mdeviceid;

@Component({
  selector: 'page-telbook',
  templateUrl: 'telbook.html'
})

export class TelbookPage {
  refreshInterval = 3000;
  refreshTask: any;
  user: any = {};
  public person: any;
  allphones:any =[];
  phoneId:any;

  buttonLabels = ['编辑','删除'];
  options: ActionSheetOptions = {
    title: '选择操作',
    subtitle: 'Choose an action',
    buttonLabels: this.buttonLabels,
    addCancelButtonWithLabel: '返回',
    addDestructiveButtonWithLabel: '取消',
    androidTheme:5,
//主题是1到5，个人喜欢5 颜色黑字白底 简约
    destructiveButtonLast: true
  };
  constructor(private rest: RestService,
    public navParams: NavParams,
    public events: Events,
    public translate: TranslateService,
    private actionSheet: ActionSheet,
    public navCtrl: NavController) {
    this.person = navParams.data.person;
    console.log(this.person);
  }

  navBack() {
    this.navCtrl.pop();
  }

  nav2edit(action) {
    this.navCtrl.push(PhoneeditPage, {
      data: this.person,
      phoneId:this.phoneId,
      allphones:this.allphones,
      action:action
    });
  }

  phoneedit(myphone){
    console.log(myphone);
    this.actionSheet.show(this.options).then((buttonIndex: number) => {
      console.log('Button pressed: ' + buttonIndex);
      //1 编辑 2 删除 3 取消
      if(buttonIndex==1){
        this.nav2edit(myphone.index);
      }
      if(buttonIndex==2){
        this.deletePhone(myphone.index);
      }
    });
  }
  deletePhone(index){
    this.allphones.splice(index-1, 1);

    let pphone ="";
    for(let i=0;i<this.allphones.length;i++){
      let mmphone = this.allphones[i];
      pphone = pphone+ mmphone.phone +","+mmphone.name+",";
    }
    pphone = pphone.substring(0, pphone.length - 1);
    mdeviceid = this.person.device2Id;
    this.rest.patch('/data/phone/' + this.phoneId , {
      devId:this.person.device2Id,
      phoneBook:pphone
    }).subscribe(resp => {
      this.rest.get("/api/phonebook/setPhb?devId=" + mdeviceid).subscribe(resp => {

      });
      this.getList();
    }, message => {
      console.log('modifyPersonError', message);
      // this.refreshSearch();
    });
  }
  getList(){
    this.rest.getWithParams('/data/phone/search/findByDevId', {
      devId: this.person.device2Id
    }).subscribe(resp => {
      console.log(resp['_embedded']['phones'].length);
      if(resp['_embedded']['phones'].length==0){
        return;
      }
      let myphone = resp['_embedded']['phones'][0].phoneBook;
      this.phoneId = resp['_embedded']['phones'][0].id;
      console.log(myphone);
      this.person.allphones=myphone;
      var pArr = myphone.split(',');
      console.log(pArr);
      this.allphones=[];
      for(let i=0;i<pArr.length/2;i++){
        let mphone = {index:0,phone:'',name:''};
        mphone.index = i+1;
        mphone.phone=pArr[i*2];
        mphone.name= pArr[i*2+1];
        this.allphones.push(mphone);
      }
      console.log(this.allphones);
    }, message => {
      console.error('Failed to get all persons in: ' + message);
    });
  }

  ionViewDidLoad() {
    this.getList();
    this.events.subscribe('reloadNotePage',() => {
      this.getList();
      });
  }

  ngOnDestroy() {
    if (this.refreshTask) {
      clearInterval(this.refreshTask);
    }
  }
}
