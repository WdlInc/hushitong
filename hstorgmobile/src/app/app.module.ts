import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { LoginPageModule } from '../pages/login/login.module';
import { NgxEchartsModule } from 'ngx-echarts';
import { Camera } from '@ionic-native/camera';
import { NativeAudio } from '@ionic-native/native-audio';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { PersonPage } from '../pages/person/person';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { AlarmPage } from '../pages/alarm/alarm';
import { HelpPage } from '../pages/help/help';
import { HelpSuggestPage } from '../pages/helpsuggest/helpsuggest';
import { UsPage } from '../pages/us/us';
import { LanguagePage } from '../pages/language/language';
import { TabshelpPage } from '../pages/tabshelp/tabshelp';
import { SearchPage } from '../pages/search/search';

import { MapPage } from '../pages/map/map';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { RestService } from './services/rest.service';
import { SafeHtmlPipe } from './services/safeHtml.pipe';
import { InfoPage } from '../pages/map/window/info';

export function createTranslateHttpLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    SafeHtmlPipe,
    PersonPage,
    ContactPage,
    HomePage,
    AlarmPage,
    HelpPage,
    HelpSuggestPage,
    UsPage,
    LanguagePage,
    TabshelpPage,
    SearchPage,
    MapPage,
    InfoPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    LoginPageModule,
    NgxEchartsModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      mode: 'ios',
      tabsHideOnSubPages: true
    }),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateHttpLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PersonPage,
    ContactPage,
    HomePage,
    AlarmPage,
    HelpPage,
    HelpSuggestPage,
    UsPage,
    LanguagePage,
    TabshelpPage,
    SearchPage,
    MapPage,
    InfoPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    RestService,
    Camera,
    NativeAudio,
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
