import { Component } from '@angular/core';

import { HelpPage } from '../help/help';
import { HelpSuggestPage } from '../helpsuggest/helpsuggest';
import { NavController } from 'ionic-angular';

@Component({
  templateUrl: 'tabshelp.html'
})
export class TabshelpPage {
  tab1Root = HelpPage;
  tab2Root = HelpSuggestPage;

  constructor(public navCtrl: NavController) {
  }

  navBack() {
    this.navCtrl.pop();
  }
}
