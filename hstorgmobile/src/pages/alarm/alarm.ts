import { Component } from '@angular/core';
import { NavController, AlertController, Nav, NavParams } from 'ionic-angular';
import { RestService } from '../../app/services/rest.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-alarm',
  templateUrl: 'alarm.html'
})
export class AlarmPage {
  user: any;
  person: any;
  alarm = '';
  alarms: any[];
  alarmTitle = '';

  constructor(private rest: RestService,
              private translate: TranslateService,
              public alertCtrl: AlertController,
              public nav: Nav,
              public navParams: NavParams,
              public navCtrl: NavController) {
    this.user = this.rest.getUser();
    this.person = navParams.data.person;
    this.alarm = navParams.data.alarm;
    this.alarmTitle = this.translate.get(this.alarm)['value'];
    this.alarms = [];
  }

  getTimeFromLast8Am() {
    let hour = new Date().getHours();
    hour = hour >= 8 ? (hour - 8) : (24 + hour - 8);
    const seconds = hour * 60 * 60 + new Date().getMinutes() * 60 + new Date().getSeconds() - 1;
    const result = new Date().getTime() - 1000 * seconds;
    return result;
  }

  getAlarmImage() {
    switch (this.alarm) {
    case "MOVE":
      return '../../assets/imgs/move.png';
    case "AWAY":
      return '../../assets/imgs/leave.png';
    case "WET":
      return '../../assets/imgs/wet.png';
    case "HEART":
      return '../../assets/imgs/heart.png';
    case "BREATHE":
      return '../../assets/imgs/breath.png';
    }
  }

  getAlarmTimes() {
    if (this.alarm !== 'MOVE') {
      return this.alarms.length;
    }

    let times = 0;
    this.alarms.forEach(item => {
      times += item.value;
    });

    return times;
  }

  ionViewCanEnter(): boolean {
    return true;
  }

  ionViewDidEnter() {
    if (this.alarm === 'MOVE') {
      this.rest.getWithParams('/data/moves', {
        personId: this.person.id,
        from: this.getTimeFromLast8Am()
      }).subscribe(resp => {
        resp.forEach(item => {
          this.alarms.push({
            'time': item,
            'value': 1
          });
        });
      }, message => {
      });
    } else {
      this.rest.getWithParams('/data/alarm/search/findAlarmTypeAfter', {
        institutionId: this.user.instId,
        personId: this.person.id,
        alarm: this.rest.getAlarmEnumNumber(this.alarm),
        from: this.getTimeFromLast8Am()
      }).subscribe(resp => {
        const items = resp['_embedded']['alarms'];
        items.forEach(item => {
          this.alarms.push({
            "time": item['raisedTime'],
            "value": item['value']
          });
        });
      }, message => {
      });
    }
  }

  popFakeValues() {
      this.alarms.push({
        "time": 1545680727000,
        "value": 1
      });
      this.alarms.push({
        "time": 1545690727000,
        "value": 2
      });
      this.alarms.push({
        "time": 1545697727000,
        "value": 1
      });
  }

  navBack() {
    this.navCtrl.pop();
  }

  ionViewWillLeave() {
  }
}
