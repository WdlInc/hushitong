import { Component } from '@angular/core';
import { NavController, AlertController, Nav,NavParams } from 'ionic-angular';
import { RestService } from '../../app/services/rest.service';
import { LoginPage } from '../login/login';
import { LanguagePage } from '../language/language';
import { TabshelpPage } from '../tabshelp/tabshelp';
import { UsPage } from '../us/us';
import { MapPage} from '../map/map'
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  user: any = {};
  persons:any;
  constructor(public navCtrl: NavController,
    private nav: Nav,
    public translate: TranslateService,
    private alerCtrl: AlertController,
    public navParams: NavParams,
    private rest: RestService) {
    this.user = this.rest.getUser();
    this.persons = navParams.data.persons;
  }

  navBack() {
    this.navCtrl.pop();
  }

  logout() {
    let confirm = this.alerCtrl.create({
      title: this.translate.get('confirmLogout')['value'],
      message: '',
      buttons: [
        {
          text: this.translate.get('no')['value'],
          handler: () => {
            console.log('Disagree clicked');
          }
        },
        {
          text: this.translate.get('yes')['value'],
          handler: () => {
            this.rest.clearup();
            this.nav.setRoot(LoginPage);
            console.log('Agree clicked');
          }
        }
      ]
    });
    confirm.present()
  }
  nav2lang() {
    this.navCtrl.push(LanguagePage, {});
  }

  nav2helpsuggest() {
    this.navCtrl.push(TabshelpPage, {});
  }

  nav2us() {
    this.navCtrl.push(UsPage, {});
  }

  nav2map(){
    console.log(this.persons);
    this.navCtrl.push(MapPage,{
      persons:this.persons
    });
  }
}
