package com.wdl.webserver.api;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/api/phonebook")
public class PhoneBookApiController {
	@Autowired
	public RestTemplate restTemplate;

	@Value("${zuul.routes.backend.url}")
	String datarest;

	@GetMapping(value = "/setPhb")
	public String setPhb(@RequestParam("devId") String devId, HttpServletRequest request) {
		String result = restTemplate.getForObject(this.datarest + "/phb/setPhoneBook?devId=" + devId, String.class);
		return result;
	}
}
