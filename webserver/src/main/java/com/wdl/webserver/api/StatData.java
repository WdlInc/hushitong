package com.wdl.webserver.api;

public class StatData {
	private Object maxHeart;
	private Object maxHeartTime;
	private Object minHeart;
	private Object minHeartTime;
	private Object maxHv;
	private Object maxHvTime;
	private Object minHv;
	private Object minHvTime;
	private Object maxLv;
	private Object maxLvTime;
	private Object minLv;
	private Object minLvTime;
	private Object meanHeart;
	private Object meanHv;
	private Object meanLv;

	public Object getMaxHeart() {
		return maxHeart;
	}

	public void setMaxHeart(Object maxHeart) {
		this.maxHeart = maxHeart;
	}

	public Object getMaxHeartTime() {
		return maxHeartTime;
	}

	public void setMaxHeartTime(Object maxHeartTime) {
		this.maxHeartTime = maxHeartTime;
	}

	public Object getMinHeart() {
		return minHeart;
	}

	public void setMinHeart(Object minHeart) {
		this.minHeart = minHeart;
	}

	public Object getMinHeartTime() {
		return minHeartTime;
	}

	public void setMinHeartTime(Object minHeartTime) {
		this.minHeartTime = minHeartTime;
	}

	public Object getMaxHv() {
		return maxHv;
	}

	public void setMaxHv(Object maxHv) {
		this.maxHv = maxHv;
	}

	public Object getMaxHvTime() {
		return maxHvTime;
	}

	public void setMaxHvTime(Object maxHvTime) {
		this.maxHvTime = maxHvTime;
	}

	public Object getMinHv() {
		return minHv;
	}

	public void setMinHv(Object minHv) {
		this.minHv = minHv;
	}

	public Object getMinHvTime() {
		return minHvTime;
	}

	public void setMinHvTime(Object minHvTime) {
		this.minHvTime = minHvTime;
	}

	public Object getMaxLv() {
		return maxLv;
	}

	public void setMaxLv(Object maxLv) {
		this.maxLv = maxLv;
	}

	public Object getMaxLvTime() {
		return maxLvTime;
	}

	public void setMaxLvTime(Object maxLvTime) {
		this.maxLvTime = maxLvTime;
	}

	public Object getMinLv() {
		return minLv;
	}

	public void setMinLv(Object minLv) {
		this.minLv = minLv;
	}

	public Object getMinLvTime() {
		return minLvTime;
	}

	public void setMinLvTime(Object minLvTime) {
		this.minLvTime = minLvTime;
	}

	public Object getMeanHeart() {
		return meanHeart;
	}

	public void setMeanHeart(Object meanHeart) {
		this.meanHeart = meanHeart;
	}

	public Object getMeanHv() {
		return meanHv;
	}

	public void setMeanHv(Object meanHv) {
		this.meanHv = meanHv;
	}

	public Object getMeanLv() {
		return meanLv;
	}

	public void setMeanLv(Object meanLv) {
		this.meanLv = meanLv;
	}

}
