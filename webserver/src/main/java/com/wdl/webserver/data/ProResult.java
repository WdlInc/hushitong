package com.wdl.webserver.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProResult {
    private ProMetric metric;
    private Object[] value;

    public ProMetric getMetric() {
        return metric;
    }

    public void setMetric(ProMetric metric) {
        this.metric = metric;
    }

    public Object[] getValue() {
        return value;
    }

    public void setValue(Object[] value) {
        this.value = value;
    }
}