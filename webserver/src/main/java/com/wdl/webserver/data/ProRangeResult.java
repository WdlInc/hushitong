package com.wdl.webserver.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProRangeResult {
    private ProMetric metric;
    private Object[] values;

    public ProMetric getMetric() {
        return metric;
    }

    public void setMetric(ProMetric metric) {
        this.metric = metric;
    }

    public Object[] getValues() {
        return values;
    }

    public void setValues(Object[] values) {
        this.values = values;
    }
}