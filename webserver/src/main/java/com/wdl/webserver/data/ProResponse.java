package com.wdl.webserver.data;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProResponse {
    private static final Logger LOG = LoggerFactory.getLogger(ProResponse.class);

    private String status;
    private ProData data;

    public boolean isSuccess() {
        return "success".equals(this.status);
    }

    public long getTime() {
        if (this.getData().getResult().length == 0) {
            return 0;
        }

        Object[] values = this.getData().getResult()[0].getValue();
        if (values.length == 2) {
            try {
                return Double.valueOf(String.valueOf(values[0])).longValue();
            } catch (Exception e) {
                LOG.error("Failed to parse long: " + values[0]);
                return 0;
            }
        }

        return 0;
    }

    public long getCounter() {
        if (this.getData().getResult().length == 0) {
            return 0;
        }

        Object[] values = this.getData().getResult()[0].getValue();
        if (values.length == 2) {
            try {
                return Double.valueOf(String.valueOf(values[1])).longValue();
            } catch (Exception e) {
                LOG.error("Failed to parse long: " + values[1]);
                return 0;
            }
        }

        return 0;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ProData getData() {
        return data;
    }

    public void setData(ProData data) {
        this.data = data;
    }
}
