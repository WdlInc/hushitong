package com.wdl.webserver.data;

public enum AlarmType {
    HEART, BREATHE, UP, SIDE, AWAY, WET, MOVE, DEVICE, RING, LEFT, RIGHT,
    // below is not for alarm type, just to record the data types in elasticsearch
    HIST_HEART, HIST_BREATHE, HIST_MOVE, SLEEP
}
