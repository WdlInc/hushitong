package com.wdl.webserver.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProData {
    private ProResult[] result;
    private String resultType;

    public ProResult[] getResult() {
        return result;
    }

    public void setResult(ProResult[] result) {
        this.result = result;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
}
