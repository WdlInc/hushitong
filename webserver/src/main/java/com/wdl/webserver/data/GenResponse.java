package com.wdl.webserver.data;

import javax.ws.rs.core.Response.StatusType;

public class GenResponse {
    int status;
    StatusType statusInfo;
    String info = "";

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public StatusType getStatusInfo() {
        return statusInfo;
    }

    public void setStatusInfo(StatusType statusInfo) {
        this.statusInfo = statusInfo;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
