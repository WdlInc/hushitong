package com.wdl.webserver.data;

import java.util.ArrayList;
import java.util.List;

import com.aliyuncs.utils.StringUtils;

public class UserBase {
    private String email;
    private boolean enabled;
    private String firstName;
    private String lastName;
    private String username;
    private String role;
    private List<String> cares = new ArrayList<String>();

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<String> getCares() {
        return cares;
    }

    public void setCares(List<String> cares) {
        this.cares = cares;
    }

    public boolean hasCares() {
        return !this.cares.isEmpty();
    }

    public void addPerson(String personId) {
        if (!StringUtils.isEmpty(personId) && !this.cares.contains(personId)) {
            this.cares.add(personId);
        }
    }

    public void removePerson(String personId) {
        if (!StringUtils.isEmpty(personId)) {
            int index = this.cares.indexOf(personId);
            if (index > -1) {
                this.cares.remove(index);
            }
        }
    }
}

