package com.wdl.webserver.data;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ProRangeData {
    private ProRangeResult[] result;
    private String resultType;

    public ProRangeResult[] getResult() {
        return result;
    }

    public void setResult(ProRangeResult[] result) {
        this.result = result;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }
}