package com.wdl.webserver.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

@Component
public class WebuiApplicationContextListener implements ApplicationListener<ContextRefreshedEvent>{
    private static final Logger LOGGER = LoggerFactory.getLogger("WebuiApplicationContextListener");

    @Value("${webui.config.path.source:file:./source/}")
    String destPath;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        LOGGER.info("Web application " + event.getApplicationContext().getApplicationName() + " was loaded, start any initialization if needed!");
    }
}
