package com.wdl.webserver;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {
    public static String getLogPrefix(HttpServletRequest request) {
        String clientIp = request.getRemoteAddr();
        String user = SecurityContextHolder.getContext().getAuthentication().getName();
        String uri = request.getRequestURI();
        return "User [" + user + "] from Host [" + clientIp + "] " + request.getMethod() + " [" + uri + "] ";
    }
}
