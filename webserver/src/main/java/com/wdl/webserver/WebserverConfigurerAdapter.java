package com.wdl.webserver;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.InMemorySwaggerResourcesProvider;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class WebserverConfigurerAdapter extends WebMvcConfigurationSupport {
    private static final Logger LOG = LoggerFactory.getLogger("WebserverConfigurerAdapter");

    @Value("${webserver.config.path.source:file:./source/}")
    String sourceFilePath;

    @Value("${webserver.config.path.static:file:./static/}")
    String staticFilePath;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        LOG.info("=== Set source path for /webui/** to " + sourceFilePath);
        registry.addResourceHandler("/webui/**")
          .addResourceLocations(sourceFilePath)
          .setCachePeriod(3600*24*7);

        LOG.info("=== Set static file path for /** to " + staticFilePath);
        registry.addResourceHandler("/**")
          .addResourceLocations(staticFilePath)
          .setCachePeriod(3600*24*30);

        registry.addResourceHandler("swagger-ui.html")
          .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
          .addResourceLocations("classpath:/META-INF/resources/webjars/");

        super.addResourceHandlers(registry);
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addRedirectViewController("/", "index.html");
    }

    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            .apis(RequestHandlerSelectors.basePackage("com.wdl.webserver"))
            .paths(PathSelectors.any())
            .build();
    }

    @Primary
    @Bean
    public SwaggerResourcesProvider dataApiResourcesProvider(InMemorySwaggerResourcesProvider defaultResourcesProvider) {
        return () -> {
            SwaggerResource resource = new SwaggerResource();
            resource.setName("Data API");
            resource.setLocation("/webui/datarest.json");

            List<SwaggerResource> resources = new ArrayList<>(defaultResourcesProvider.get());
            resources.add(resource);
            return resources;
        };
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
            .title("Hushitong API Documentation")
            .description("2019.01.25")
            .version("1.0.0")
            .build();
    }
}
