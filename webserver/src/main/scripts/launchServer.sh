#!/bin/sh
# set -x
#########################################################################
# Date: 2018/08/11
# Description: This script is to launch the main process
#########################################################################

cd `dirname $0`
basedir=`pwd`

unameOut="$(uname -s)"
case "${unameOut}" in
  CYGWIN*)
    # replace /cygdrive/d/ with d:/
    basedir=$(echo ${basedir} | sed -e "s/\/cygdrive\///" | sed -e "s/\//:\//")
    ;;
  *)
esac

PROCESS_NAME="webserver"

# Run the process with default settings in application.yml.
run() {
echo "${0}: ${PROCESS_NAME} is starting from ${basedir} with settings in application.yml..."

if test -n "${JAVA_HOME}"; then
  exec ${JAVA_HOME}/bin/java \
    -Xmx1024m \
    -server \
    -DProcess=${PROCESS_NAME} \
    -Dspring.config.location=file:${basedir}/config/ \
    -Dlogging.config=file:${basedir}/config/logback.xml \
    -jar ${basedir}/lib/${PROCESS_NAME}.jar \
    >> ${basedir}/../logs/${PROCESS_NAME}.stdout 2>> ${basedir}/../logs/${PROCESS_NAME}.stderr &
else
  exec /usr/bin/java \
    -Xmx1024m \
    -server \
    -DProcess=${PROCESS_NAME} \
    -Dspring.config.location=file:${basedir}/config/ \
    -Dlogging.config=file:${basedir}/config/logback.xml \
    -jar ${basedir}/lib/${PROCESS_NAME}.jar \
    >> ${basedir}/../logs/${PROCESS_NAME}.stdout 2>> ${basedir}/../logs/${PROCESS_NAME}.stderr &
fi
}

case $1 in
  *)
    run;
    ;;
esac
