<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow">

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title>
        <#nested "title">
    </title>
    <link rel="icon" href="${url.resourcesPath}/img/favicon.ico" />
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <script type="text/javascript" src="${url.resourcesPath}/js/jquery.min.js" ></script>

    <script type="text/javascript">
       $(document).ready(function() {

            $("select").click(function() {
              var open = $(this).data("isopen");
              if(open) {
                window.location.href = $(this).val()
              }
              //set isopen to opposite so next time when use clicked select box
              //it wont trigger this event
              $(this).data("isopen", !open);
            });

            $(function() {
                var empty = false;

                function check_inputs() {
                    $('#sbtbtn').attr('disabled', 'disabled');

                    if ($('#target1').val() !==''&&
                    $('#target2').val() !== '') {
                        $('#sbtbtn').removeAttr('disabled');
                        empty = true;
                    } else {
                        $('#sbtbtn').attr('disabled', 'disabled');
                        empty = false;
                    }

                    if (empty === false) {

                        $('form input[type=submit]').css({
                                                        'color':'rgba(0,0,0,0.26)',
                                                        'background-color': 'rgba(79,79,79,0.26)'
                                                    });
                    } else {
                        $('form input[type=submit]').css({
                                                        'color': '#FFFFFF',
                                                        'background-color': '#124191'
                                                    });
                    }
                }

                $('form input[type=text], form input[type=password]').keyup(function() {
                    check_inputs();
                });
            });
        });
    </script>

</head>

<body id="bck-img" class="${properties.kcBodyClass!}" style="margin: 0;">
    <div id="kc-container" class="${properties.kcContainerClass!}">
        <div id="kc-container-wrapper" class="${properties.kcContainerWrapperClass!}">

            <div id="kc-header" class="${properties.kcHeaderClass!}" style="background: rgba(18, 65, 145, 0.7);">
                <div>
                    <div id="kc-header-wrapper" class="${properties.kcHeaderWrapperClass!}">
                        <#nested "header">
                    </div>
                </div>
            </div>

            <div id="kc-content" class="${properties.kcContentClass!}">
                        <div id="notification-area" class="login__notification-area--container">
                        <#if displayMessage && message?has_content>
                            <div class="${properties.kcFeedbackAreaClass!}">
                                <div class="login__icon-container">
                                    <#if message.type = 'success'>
                                        <span><img class="img-icon" src="${url.resourcesPath}/img/ic_info_blue_24px.svg"/></span>
                                    </#if>
                                    <#if message.type = 'warning'>
                                        <span><img class="img-icon" src="${url.resourcesPath}/img/ic_warning_yellow_24px.svg"/></span>
                                    </#if>
                                    <#if message.type = 'error'>
                                        <span><img class="img-icon" src="${url.resourcesPath}/img/ic_error_red_24px.svg"/></span>
                                    </#if>
                                    <#if message.type = 'info'>
                                        <span><img class="img-icon" src="${url.resourcesPath}/img/ic_info_blue_24px.svg"/></span>
                                    </#if>
                                </div>
                                <#if message.type = 'error'>
                                    <p class="error-message" style="height: 24px;">${message.summary?no_esc}</p>
                                <#else>
                                    <p class="notification-message style="height: 24px;">${message.summary?no_esc}</p>
                                </#if>
                            </div>
                        </#if>
                        </div>
                        <div id="kc-form" class="${properties.kcFormAreaClass!}">
                            <div id="kc-form-wrapper" class="${properties.kcFormAreaWrapperClass!}">
                                <#nested "form">
                            </div>
                        </div>
                        <div id="lang" class="login__language-selection csfWidgets">
                            <#if realm.internationalizationEnabled>
                                <div id="kc-locale" class="${properties.kcLocaleClass!}">
                                        <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                                            <div class="kc-dropdown" id="kc-locale-dropdown">
                                                <span class="language-label">Language:</span>
                                                <select id="kc-select-drpdwn" onchange="locale.supported = this.value;">
                                                    <#list locale.supported as l>
                                                        <option class="kc-dropdown-item" value="${l.url}" <#if (l.label == locale.current)>
                                                        selected
                                                        </#if>>
                                                        ${l.label}</option>
                                                    </#list>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                            </#if>
                        </div>

            </div>
            <div>
                <span class="login__version-number--is-mobile">${client.description!''}</span>
                <span class="login__copyright-text--is-mobile">© 2018 Hushitong</span>
            </div>
        </div>
    </div>
</body>
</html>
</#macro>
