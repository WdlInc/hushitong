<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=social.displayInfo; section>
    <#if section = "title">
        ${msg("loginTitle",(realm.displayName!''))}
    <#elseif section = "header">
    <div id="text-content" class="text-content">
        <div id="kc-logo" class="image-wrapper">
            <a href="${properties.kcLogoLink!'#'}">
                <div id="kc-logo-wrapper">
                    <img src="${url.resourcesPath}/img/logo_48.png" class="img"/>
                </div>
            </a>
        </div>
        <div id="pro-fam-name" class="login__product-name">
                ${msg("loginTitleHtml",(realm.displayNameHtml!''))?no_esc}
        </div>

        <#if client.name?? || client.name?has_content>
          <#if client.name == r"${client_security-admin-console}">
            <div id="pro-des" class="login__description">
                    ${msg("client_security-admin-console",(client.name!''))?no_esc}
            </div>
          <#else>
            <div id="pro-des" class="login__description"> ${client.name!''} </div>
          </#if>
        </#if>

        <div id="version" class="login__version-number"> ${client.description!''} </div>
    </div>
    <div class="login__copyright-details">
        <div class="login__copyright-text">© 2018 Hushitong</div>
    </div>
    <#elseif section = "side-div">
        <div id="kc-container">

        </div>
    <#elseif section = "form">
            <#if realm.password>
                <form id="kc-form-login" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
                    <div class="${properties.kcFormGroupClass!}">
                        <div class="${properties.kcInputWrapperClass!}">
                            <div class="header-height"></div>
                            <#if usernameEditDisabled??>
                                <input tabindex="0" area-label="textinput" area-multiline="false" area-readonly="false" id="target1" id="username" id="lgn" class="${properties.kcInputClass!}" name="username" placeholder="${msg('username')}" type="text"/>
                            <#else>
                                <input tabindex="0" area-label="textinput" area-multiline="false" area-readonly="false" id="target2" id="username" id="lgn" class="${properties.kcInputClass!}" name="username" placeholder="${msg('username')}" type="text" autocomplete="off"/>
                            </#if>
                            <div class="footer-height"></div>
                        </div>
                    </div>
                    <div class="${properties.kcFormGroupClass!}">
                        <div class="${properties.kcInputWrapperClass!}">
                            <div class="header-height"></div>
                            <input tabindex="0" id="login-password" area-label="textinput" area-multiline="false" area-readonly="false" class="${properties.kcInputClass!}" name="password" placeholder="${msg('password')}" type="password" autocomplete="off" />
                            <div class="footer-height"></div>
                        </div>
                    </div>
                    <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <#if realm.rememberMe && !usernameEditDisabled??>
                            <div class="checkbox" style="width: 50%">
                                <label>
                                    <#if login.rememberMe??>
                                        <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3" class="rememberMe" checked>
                                        <div class="size">${msg("rememberMe")}</div>
                                    <#else>
                                        <input id="rememberMe" name="rememberMe" type="checkbox" tabindex="3" class="rememberMe" > <div class="size">${msg("rememberMe")}</div>
                                    </#if>
                                </label>
                            </div>
                        </#if>

                        <div class="csfWidgets login__forgot-password" style="width: 50%;">
                            <#if realm.resetPasswordAllowed>
                                <div>
                                    <a class="csfWidgets hyperlink " id="hyperlink1" href="${url.loginResetCredentialsUrl}" target="" rel="noopener noreferrer">${msg("doForgotPassword")}</a>
                                </div>
                            </#if>
                        </div>
                    </div>
                    <div id="kc-form-buttons" class="${properties.kcFormButtonsClass!}">
                        <div class="${properties.kcFormButtonsWrapperClass!}">
                            <input id="sbtbtn" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="login" id="kc-login" type="submit" value="${msg("doLogIn")}"/>
                        </div>
                    </div>
                </form>
            </#if>
    <#elseif section = "info" >
    <!-- Disable register and social login for PC!!!
        <#if realm.password && realm.registrationAllowed && !usernameEditDisabled??>
            <div id="kc-registration" >
                <span id="new-register">${msg("noAccount")} <a class="csfWidgets hyperlink" href="${url.registrationUrl}">${msg("doRegister")}</a></span>
            </div>
        </#if>

        <#if realm.password && social.providers??>
            <div id="kc-social-providers">
                <ul>
                    <#list social.providers as p>
                        <li><a href="${p.loginUrl}" id="zocial-${p.alias}" class="zocial ${p.providerId}"> <span class="text">${p.displayName}</span></a></li>
                    </#list>
                </ul>
            </div>
        </#if>
    -->
    </#if>
</@layout.registrationLayout>
