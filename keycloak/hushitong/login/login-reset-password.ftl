<#import "template.ftl" as layout>
<@layout.registrationLayout displayInfo=true; section>
    <#if section = "title">
        ${msg("emailForgotTitle")}
    <#elseif section = "header">
    <div id="text-content" class="text-content">
        <div id="kc-logo" class="image-wrapper">
            <a href="${properties.kcLogoLink!'#'}">
                <div id="kc-logo-wrapper">
                    <img src="${url.resourcesPath}/img/logo_48.png" class="img"/>
                </div>
            </a>
        </div>
        <div id="pro-des" class="login__description">${msg("emailForgotTitle")}</div>
    </div>
    <div class="login__copyright-details">
        <div class="login__copyright-text">© 2018 Hushitong</div>
    </div>
    <#elseif section = "form">
        <form id="kc-reset-password-form" class="${properties.kcFormClass!}" action="${url.loginAction}" method="post">
            <div class="${properties.kcFormGroupClass!}">
                
                <div class="${properties.kcInputWrapperClass!}">
                    <input type="text" id="forget-username" placeholder="${msg('usernameOrEmail')}" name="username" class="${properties.kcInputClass!}" autofocus/>
                </div>
            </div>

            <div class="${properties.kcFormGroupClass!}">
                <div id="kc-form-options" class="${properties.kcFormOptionsClass!}">
                    <div style="margin: 10px 0 0 0;" class="${properties.kcFormOptionsWrapperClass!}">
                        <span><a class="csfWidgets hyperlink" href="${url.loginUrl}">${msg("backToLogin")}</a></span>
                    </div>
                </div>

                <div id="kc-form-buttons" class="kc-form-button" class="${properties.kcFormButtonsClass!}">
                    <input class="register-submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" type="submit" value="${msg('doSubmit')}"/>
                </div>
            </div>
        </form>
    <#elseif section = "info" >
        ${msg("emailInstruction")}
    </#if>
</@layout.registrationLayout>
