/**
 * Go to below page for more options:
 *   https://github.com/chimurai/http-proxy-middleware
 */
const PROXY_CONFIG = [
/*
{
  context: ['/api/keycloak/**'],
  target: "https://192.168.123.235:8443",
  secure: false,
},
*/
/*
{
  context: ['/auth/**'],
  target: "https://192.168.123.235:8443",
  secure: false,
  "logLevel": "debug",
},*/
{
  context: ['/api/v1/**'],
 // target: "http://192.168.123.235:9090",47.105.79.216，120.27.13.7
  target: "https://192.168.123.200",
  secure: false,
},
{
  context: ['/api/pro/**'],
  //target: "https://192.168.123.30",
 target: "https://192.168.123.200",
  changeOrigin:true,
  secure: false,
  "logLevel": "debug",
},
{
  context: ['/api/influx/**'],
  target: "https://192.168.123.200",
  secure: false,
},
{
  context: ['/data/**'],
  target: "https://192.168.123.200",
  secure: false,
  // ws: false,
  changeOrigin: true,
  // xfwd: false,
  logLevel: 'debug',
  bypass: function(req, res, proxyOptions) {
    /*
    if (req.headers.accept.indexOf('html') !== 01) {
      console.log('Skpping proxy for browser request.');
      return '/index.html';
    }
    */
}}]

module.exports = PROXY_CONFIG;
