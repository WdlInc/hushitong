import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { KeycloakService } from './app/services/keycloak.service'
import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

declare var $: any;

if (environment.production) {
  enableProdMode();
}

const bootstrap = () => {
  platformBrowserDynamic().bootstrapModule(AppModule);
}

let baseUri = document.baseURI;
if (!baseUri) {
  const baseTags = document.getElementsByTagName('base');
  baseUri = baseTags.length > 0 ? baseTags[0].href : document.URL;
}

$.ajax(baseUri + (environment.debugging ? 'assets/samples/' : '') + 'webui/systemInfo.json').done(data => {
  environment.version = data.version;
  environment.doLoginFlag = data.doLoginFlag !== false;
  environment.logoutWhenAuthFail = data.logoutWhenAuthFail !== false;
  environment.realm = data.realm;
  environment.resource = data.resource;
  environment.interval = data.interval ? data.interval : 3000;

  KeycloakService.init()
  .then(() => bootstrap())
  .catch (e => {
    console.error(e);
  });
}).fail(message => {
  console.error('Failed to retrieve systemInfo.json, default to init without Keycloak!');
  bootstrap();
});
