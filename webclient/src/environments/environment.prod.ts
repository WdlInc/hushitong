export const environment = {
  production: true,
  debugging: false,
  doLoginFlag: true,
  logoutWhenAuthFail: true,
  locale: 'zh',
  version: '1.0.0',
  realm: 'hushitong',
  resource: 'webclient',
  interval: 3000
};
