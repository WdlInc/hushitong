import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { BedRoutingModule } from './bed-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { BedComponent } from './bed.component';
import { BedDialogComponent } from './bed-dialog/bed-dialog.component';
import { BobjectComponent } from './bobject/bobject.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DraggableModule,
    BedRoutingModule,
    TranslateModule
  ],
  declarations: [
    BedComponent,
    BedDialogComponent,
    BobjectComponent
  ],
  entryComponents: [
    BedDialogComponent
  ]
})
export class BedModule { }
