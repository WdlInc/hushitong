import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { HomeComponent } from './home.component';
import { SystemInfoDialogComponent } from './system-info-dialog/system-info-dialog.component';
import { UserInfoDialogComponent } from './user-info-dialog/user-info-dialog.component';
import { BselectionComponent } from './bselection/bselection.component';
import { BfilterComponent } from './bfilter/bfilter.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    DraggableModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
    TranslateModule
  ],
  declarations: [
    HomeComponent,
    SystemInfoDialogComponent,
    UserInfoDialogComponent,
    BselectionComponent,
    BfilterComponent
  ],
  entryComponents: [
    SystemInfoDialogComponent,
    UserInfoDialogComponent,
  ],
  exports: [
    BselectionComponent,
    BfilterComponent
  ]
})
export class HomeModule { }
