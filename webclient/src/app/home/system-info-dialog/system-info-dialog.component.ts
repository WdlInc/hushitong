import { AfterViewInit, Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { WidgetsService } from './../../services/widgets.service';
import { RestService } from '../../services/rest.service';
import { DataService } from './../../services/data.service';

@Component({
  selector: 'app-system-info-dialog',
  templateUrl: './system-info-dialog.component.html',
  styleUrls: ['./system-info-dialog.component.css']
})
export class SystemInfoDialogComponent implements AfterViewInit {
  user: any;
  server;

  contentHeight = '65vh';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<SystemInfoDialogComponent>,
              private dialog: MatDialog,
              private restService: RestService,
              private dataService: DataService,
              private widgets: WidgetsService) {
    this.user = this.widgets.getUser();
    this.server = this.widgets.getServer(null);
    // We need to keep the current sni map since this dialog could be opened at any time.
  }

  ngAfterViewInit() {
  }
}
