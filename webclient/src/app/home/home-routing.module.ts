import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home.component';

const homeRoutes: Routes = [
  { path: 'home',
    component: HomeComponent,
    children: [
      { path: 'mcenter', loadChildren: '../mcenter/mcenter.module#McenterModule' },
      { path: 'map', loadChildren: '../map/map.module#MapModule' },
      { path: 'monitor', loadChildren: '../monitor/monitor.module#MonitorModule' },
      { path: 'person', loadChildren: '../person/person.module#PersonModule' },
      { path: 'device', loadChildren: '../device/device.module#DeviceModule' },
      { path: 'alarm', loadChildren: '../alarm/alarm.module#AlarmModule' },
      { path: 'nurse', loadChildren: '../nurse/nurse.module#NurseModule' },
      { path: 'bed', loadChildren: '../bed/bed.module#BedModule' },
      { path: 'institution', loadChildren: '../institution/institution.module#InstitutionModule' },
      { path: 'department', loadChildren: '../department/department.module#DepartmentModule' },
      { path: 'user', loadChildren: '../user/user.module#UserModule' },
      { path: 'menu', loadChildren: '../menu/menu.module#MenuModule' },
      // { path: '', loadChildren: '../mcenter/mcenter.module#McenterModule' }
    ]
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(homeRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class HomeRoutingModule { }
