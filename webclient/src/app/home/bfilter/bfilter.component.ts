import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { RestService } from '../../services/rest.service';
import { TranslateService } from '@ngx-translate/core';

import * as _ from 'lodash';

@Component({
  selector: 'app-bfilter',
  templateUrl: './bfilter.component.html',
  styleUrls: ['./bfilter.component.css']
})
export class BfilterComponent implements OnInit {
  @Output() change = new EventEmitter<any>();

  institution: any;
  allString = 'All';

  buildings = []; 
  levels = [];
  rooms = [];
  nurses = [];

  bedData = {
    building: -1,
    level: -1,
    room: -1,
    bedString: '',
    nurse: -1
  };

  constructor(private rest: RestService, private translate: TranslateService) {
    this.institution = this.rest.getInstitution();
    this.allString = this.translate.get('all')['value'];
  }

  buildingChanges(event) {
    // building has changed, reset to default values
    this.levels = [{
      id: -1,
      levelName: this.allString
    }];

    this.rooms = [{
      id: -1,
      roomName: this.allString
    }];

    this.bedData.building = event.value;
    this.bedData.level = -1;
    this.bedData.room = -1;

    this.fireEvent();

    // get levels if building is valid
    if (event.value > 0) {
      this.rest.get('data/building/' + event.value + '/levels').subscribe(data => {
        this.levels = data['_embedded']['levels'];
        this.levels.unshift({id: -1, levelName: this.allString});
        this.levelChanges({value: -1});
      });
    }
  }

  levelChanges(event) {
    this.rooms = [{
      id: -1,
      roomName: this.allString
    }];

    this.bedData.level = event.value;
    this.bedData.room = -1;

    this.fireEvent();

    if (event.value > 0) {
      this.rest.get('data/level/' + event.value + '/rooms').subscribe(data => {
        this.rooms = data['_embedded']['rooms'];
        this.rooms.unshift({id: -1, roomName: this.allString});
        this.roomChanges({value: -1});
      });
    }
  }

  roomChanges(event) {
    this.bedData.room = event.value;
    this.fireEvent();
  }

  fireEvent() {
    const beds = [];

    if (this.bedData.building > 0) {
      const index = _.findIndex(this.buildings, {id: this.bedData.building});
      if (index > -1) {
        beds.push(this.buildings[index].buildingName);
      }
    }

    if (this.bedData.level > 0) {
      const index = _.findIndex(this.levels, {id: this.bedData.level});
      if (index > -1) {
        beds.push(this.levels[index].levelName);
      }
    }

    if (this.bedData.room > 0) {
      const index = _.findIndex(this.rooms, {id: this.bedData.room});
      if (index > -1) {
        beds.push(this.rooms[index].roomName);
      }
    }

    if (beds.length > 0) {
      this.bedData.bedString = beds.join('##');
    } else {
      this.bedData.bedString = '';
    }

    this.change.emit(this.bedData);
  }

  nurseSelected(event) {
    this.bedData.nurse = event.value;
    this.fireEvent();
  }

  ngOnInit() {
    this.rest.get('data/institution/' + this.institution.id + '/buildings').subscribe(data => {
      this.buildings = data['_embedded']['buildings'];
      this.buildings.unshift({id: -1, buildingName: this.allString});
      this.buildingChanges({value: -1});
    });

    this.rest.get('data/institution/' + this.institution.id + '/nurses').subscribe(data => {
      this.nurses = data['_embedded']['nurses'];
      this.nurses.unshift({id: -1, nurseName: this.allString});
      this.nurseSelected({value: -1});
    });
  }
}