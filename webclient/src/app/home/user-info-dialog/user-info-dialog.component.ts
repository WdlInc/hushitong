import { Component } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { WidgetsService } from './../../services/widgets.service';

@Component({
  selector: 'app-user-info-dialog',
  templateUrl: './user-info-dialog.component.html',
  styleUrls: ['./user-info-dialog.component.css']
})
export class UserInfoDialogComponent {
  formGroup: FormGroup;

  constructor(fb: FormBuilder,
              public dialogRef: MatDialogRef<UserInfoDialogComponent>,
              private translate: TranslateService,
              private widgets: WidgetsService) {
    const user = this.widgets.getUser();

    this.formGroup = fb.group({
      userName: [ { value: user.name, disabled: true } ],
      userRole: [ { value: this.translate.get(user.role)['value'], disabled: true } ],
    });
  }
}