import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { MenuRoutingModule } from './menu-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MenuComponent } from './menu.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    DraggableModule,
    MenuRoutingModule,
    TranslateModule
  ],
  declarations: [
    MenuComponent
  ],
  entryComponents: [
  ]
})
export class MenuModule { }
