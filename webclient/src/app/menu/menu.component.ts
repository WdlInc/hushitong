import { Component } from '@angular/core';
import { RestService } from '../services/rest.service';

@Component({
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent {
  user: any;

  constructor(private rest: RestService) {
    this.user = this.rest.getUser();
  }
}
