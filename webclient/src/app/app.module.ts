import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { HomeModule } from './home/home.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { SbcmaterialModule } from './sbcmaterial/sbcmaterial.module';
import { DraggableModule } from './draggable/draggable.module';

import { MatPaginatorIntl } from '@angular/material';
import { WdlPaginatorIntl } from './services/wdlpaginator-intl';
import { WidgetsService } from './services/widgets.service';
import { RestService } from './services/rest.service';
import { LoginService } from './services/login.service';
import { DataService } from './services/data.service';

import { AppComponent } from './app.component';
import { KeycloakService } from './services/keycloak.service';
import { DialogComponent } from './dialog/dialog.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

export function createTranslateHttpLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HomeModule,
    AppRoutingModule,
    SbcmaterialModule,
    DraggableModule,
    InfiniteScrollModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateHttpLoader),
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AppComponent,
    DialogComponent
  ],
  providers: [
    WidgetsService,
    DataService,
    RestService,
    LoginService,
    KeycloakService,
    {
      provide: MatPaginatorIntl,
      useClass: WdlPaginatorIntl
    }
  ],
  entryComponents: [
    DialogComponent
  ],
  bootstrap: [AppComponent]
})

export class AppModule { };
