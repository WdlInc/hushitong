import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { MatIconRegistry} from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '../environments/environment';

@Component({
  selector: 'isbc',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent implements OnInit {
  constructor(public translateService: TranslateService, private matIconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    matIconRegistry.addSvgIcon('menu', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_menu.svg'));
    matIconRegistry.addSvgIcon('user', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_user.svg'));
    matIconRegistry.addSvgIcon('admin', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_admin_settings.svg'));
    matIconRegistry.addSvgIcon('monitor', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_dashboard.svg'));
    matIconRegistry.addSvgIcon('center', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_launch_FM.svg'));
    matIconRegistry.addSvgIcon('person', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_qos.svg'));
    matIconRegistry.addSvgIcon('device', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_virtual_machine.svg'));
    matIconRegistry.addSvgIcon('alarm', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_alarm_list.svg'));
    matIconRegistry.addSvgIcon('nurse', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_public_view.svg'));
    matIconRegistry.addSvgIcon('department', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_cluster.svg'));
    matIconRegistry.addSvgIcon('bed', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_bed.svg'));
    matIconRegistry.addSvgIcon('domain', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_domain.svg'));
    matIconRegistry.addSvgIcon('users', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_subscribers.svg'));
    matIconRegistry.addSvgIcon('rights', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_private_view.svg'));
    matIconRegistry.addSvgIcon('continue', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_import.svg'));
    matIconRegistry.addSvgIcon('error', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_error_alert.svg'));
    matIconRegistry.addSvgIcon('add', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_add.svg'));
    matIconRegistry.addSvgIcon('delete', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_delete.svg'));
    matIconRegistry.addSvgIcon('edit', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_edit.svg'));
    matIconRegistry.addSvgIcon('view', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_log.svg'));
    matIconRegistry.addSvgIcon('import', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_import.svg'));
    matIconRegistry.addSvgIcon('export', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_export.svg'));
    matIconRegistry.addSvgIcon('search', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_search_refresh.svg'));
    matIconRegistry.addSvgIcon('find', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_find.svg'));
    matIconRegistry.addSvgIcon('password', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/password.svg'));
    matIconRegistry.addSvgIcon('close', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_close.svg'));
    matIconRegistry.addSvgIcon('hide', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_hide.svg'));
    matIconRegistry.addSvgIcon('show', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_show.svg'));
    matIconRegistry.addSvgIcon('bind', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_links.svg'));
    matIconRegistry.addSvgIcon('unbind', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_links_down.svg'));
    matIconRegistry.addSvgIcon('checkout', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_export.svg'));
    matIconRegistry.addSvgIcon('ack', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_alarms_ack.svg'));
    matIconRegistry.addSvgIcon('unack', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_alarms_unack.svg'));
    matIconRegistry.addSvgIcon('resume', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_play.svg'));
    matIconRegistry.addSvgIcon('stop', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/ic_pause.svg'));
    matIconRegistry.addSvgIcon('hist', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/hist.svg'));
    matIconRegistry.addSvgIcon('position', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/position.svg'));
    matIconRegistry.addSvgIcon('showband', sanitizer.bypassSecurityTrustResourceUrl('../assets/images/band.svg'));
  }

  ngOnInit() {
    this.translateService.addLangs(['zh', 'tw', 'en']);
    this.translateService.setDefaultLang('en');

    switch (environment.locale) {
    case 'en':
      this.translateService.use('en');
      break;
    case 'zh-CN':
      this.translateService.use('zh');
      break;
    case 'zh-TW':
      this.translateService.use('tw');
      break;
    default:
      console.log('Use browser settings to set locale.');
      const browserLang = this.translateService.getBrowserLang();
      this.translateService.use(browserLang.match(/zh|en/) ? browserLang : 'zh');
      break;
    }
  }
}
