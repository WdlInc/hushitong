export class Institution {
  id: number;
  instName: string;
  description: string;
}
