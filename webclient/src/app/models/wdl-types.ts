export enum DialogTypes {
  INFO, WARNING, ERROR, CONFIRMATION
};

export enum ActionTypes {
  ADD, MODIFY, DELETE, READ
};

export enum ObjectTypes {
  INSTITUTION, BUILDING, LEVEL, ROOM, BED
};

export enum SexEnums {
  MALE, FEMAIL
};

export enum Education {
  NONE, PRIMARY, MIDDLE, COLLEGE
};