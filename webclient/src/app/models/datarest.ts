export class Datarest {
  _embedded: any;
  _links: {
    self: {
      href: string,
      templated: boolean
    },
    profile: {
      href: string
    },
    search: {
      href: string
    }
  };
  page: {
    size: number,
    totoalElements: number,
    totalPages: number,
    number: number
  };
}