import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ArchwizardModule } from 'angular-archwizard';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { McenterRoutingModule } from './mcenter-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { McenterComponent } from './mcenter.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { PersonContentComponent } from './person-content/person-content.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { CobjectComponent } from './cobject/cobject.component';
import { BandobjectComponent } from './bandobject/bandobject.component';
import { AlarmDialogComponent } from './alarm-dialog/alarm-dialog.component';
import { BhhistDialogComponent } from './bhhist-dialog/bhhist-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SbcmaterialModule,
    DraggableModule,
    McenterRoutingModule,
    TranslateModule,
    NgxEchartsModule,
    ArchwizardModule
  ],
  declarations: [
    McenterComponent,
    PersonDetailsComponent,
    CobjectComponent,
    AlarmDialogComponent,
    BhhistDialogComponent,
    BandobjectComponent,
    PersonContentComponent
  ],
  entryComponents: [
    PersonDetailsComponent,
    AlarmDialogComponent,
    BhhistDialogComponent,
    PersonContentComponent,
  ],
  exports: [
    PersonDetailsComponent
  ]
})
export class McenterModule { }
