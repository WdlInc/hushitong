import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { McenterComponent } from './mcenter.component';

const childRoutes: Routes = [
  { path: '', component: McenterComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(childRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class McenterRoutingModule { }
