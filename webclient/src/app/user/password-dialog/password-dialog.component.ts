import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';

@Component({
  selector: 'app-password-dialog',
  templateUrl: './password-dialog.component.html',
  styleUrls: ['./password-dialog.component.css']
})
export class PasswordDialogComponent {
  formGroup: FormGroup;
  item: any;
  institution: any;
  title = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PasswordDialogComponent>,
              fb: FormBuilder,
              private translate: TranslateService) {
    this.item = dialogData.item;
    this.institution = dialogData.institution;

    this.title = this.translate.get("resetPassword")['value'];
    this.formGroup = fb.group({
      username: [ '', [] ],
      password: [ '', [Validators.required] ]
    });

    this.formGroup.controls.username.setValue(this.item.username);
  }

  disableSubmit() {
    return this.formGroup.invalid || !this.formGroup.dirty;
  }

  submit() {
    this.item['password'] = this.formGroup.controls.password.value;
    this.dialogRef.close(this.item);
  }
}