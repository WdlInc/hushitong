import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { UserRoutingModule } from './user-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { UserComponent } from './user.component';
import { UserDialogComponent } from './user-dialog/user-dialog.component';
import { PasswordDialogComponent } from './password-dialog/password-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DraggableModule,
    UserRoutingModule,
    TranslateModule
  ],
  declarations: [
    UserComponent,
    UserDialogComponent,
    PasswordDialogComponent
  ],
  entryComponents: [
    UserDialogComponent,
    PasswordDialogComponent
  ]
})
export class UserModule { }
