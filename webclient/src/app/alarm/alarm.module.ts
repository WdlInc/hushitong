import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { AlarmRoutingModule } from './alarm-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { AlarmComponent } from './alarm.component';
import { ExportAlarmDialogComponent } from './export-alarm-dialog/export-alarm-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SbcmaterialModule,
    DraggableModule,
    AlarmRoutingModule,
    TranslateModule
  ],
  declarations: [
    AlarmComponent,
    ExportAlarmDialogComponent
  ],
  entryComponents: [
    ExportAlarmDialogComponent
  ],
  providers: [
    DatePipe
  ]
})
export class AlarmModule { }
