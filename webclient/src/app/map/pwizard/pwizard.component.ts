import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { SexEnums, Education, DialogTypes } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-pwizard',
  templateUrl: './pwizard.component.html',
  styleUrls: ['./pwizard.component.css']
})
export class PwizardComponents implements OnInit {
  basicForm: FormGroup;
  contactForm: FormGroup;
  healthForm: FormGroup;
  hospitalForm: FormGroup;

  institution: any;
  bed: any;
  nurses = [];
  devices = [];
  bedId = -1;

  readonly = false;
  url = '';

  searchText = '';
  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PwizardComponents>,
              private adapter: DateAdapter<any>,
              fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.institution = this.rest.getInstitution();
    this.url = 'data/institution/' + this.institution.id + '/';

    switch (this.translate.currentLang) {
    case 'en':
      this.adapter.setLocale('en');
      break;
    case 'tw':
      this.adapter.setLocale('zh-TW');
      break;
    case 'zh':
    default:
      this.adapter.setLocale('zh-CN');
      break;
    }

    this.basicForm = fb.group({
      personName: [
        { value: '', disabled: this.readonly },
        [ Validators.required, Validators.minLength(1), Validators.maxLength(40) ]
      ],
      sex: [
        { value: 'male', disabled: this.readonly },
        [ Validators.required, ]
      ],
      nation: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      education: [
        { value: 'none', disabled: this.readonly },
        []
      ],
      rid: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      birth: [
        { value: null, disabled: this.readonly },
        []
      ],
      phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
    });

    this.contactForm = fb.group({
      contact1Name: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(40) ]
      ],
      contact1Sex: [
        { value: 'male', disabled: this.readonly },
        [ ]
      ],
      contact1Phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      contact1Address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
      contact2Name: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(40) ]
      ],
      contact2Sex: [
        { value: 'male', disabled: this.readonly },
        []
      ],
      contact2Phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      contact2Address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
    });

    this.healthForm = fb.group({
      heartUp: [
        { value: 90, disabled: this.readonly },
        []
      ],
      heartLow: [
        { value: 40, disabled: this.readonly },
        []
      ],
      breatheUp: [
        { value: 25, disabled: this.readonly },
        []
      ],
      breatheLow: [
        { value: 10, disabled: this.readonly },
        []
      ],
      move: [
        { value: 10, disabled: this.readonly },
        []
      ],
      turnOverReminder: [
        { value: 0, disabled: this.readonly },
        []
      ],
      awayAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      upAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      sideAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      silent: [
        { value: false, disabled: this.readonly },
        []
      ],
    });

    this.hospitalForm = fb.group({
      checkinRecord: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(256) ]
      ],
      nurseId: [
        { value: null, disabled: this.readonly },
        []
      ],
      device1Id: [
        { value: null, disabled: this.readonly },
        []
      ],
    });
  }


  refreshSearch(){

  }

  ngOnInit() {
    this.rest.get(this.url + 'nurses').subscribe(data => {
      this.nurses = data['_embedded']['nurses'];
    });

    this.rest.get(this.url + 'devices').subscribe(data => {
      const devices = data['_embedded']['devices'];
      this.devices = [];
      devices.forEach(device => {
        if (device.personId < 1) {
          this.devices.push(device);
        }
      });
    });
  }
}
