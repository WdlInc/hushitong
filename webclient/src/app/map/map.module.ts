import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { MapRoutingModule } from './map-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MapComponent } from './map.component';
import { PersonDetailsComponent } from './person-details/person-details.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { CobjectComponent } from './cobject/cobject.component';
import { AlarmDialogComponent } from './alarm-dialog/alarm-dialog.component';
import { BhhistDialogComponent } from './bhhist-dialog/bhhist-dialog.component';

import { PwizardComponents } from './pwizard/pwizard.component';
import { ListPersonComponents } from './list-person/list-person.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SbcmaterialModule,
    DraggableModule,
    MapRoutingModule,
    TranslateModule,
    NgxEchartsModule
  ],
  declarations: [
    MapComponent,
    PersonDetailsComponent,
    CobjectComponent,
    AlarmDialogComponent,
    BhhistDialogComponent,
    PwizardComponents,
    ListPersonComponents
  ],
  entryComponents: [
    PersonDetailsComponent,
    AlarmDialogComponent,
    BhhistDialogComponent,
    PwizardComponents,
    ListPersonComponents
  ],
  exports: [
    PersonDetailsComponent,
    //PwizardComponents
  ]
})
export class MapModule { }
