import { Component, OnInit, Inject } from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { SexEnums, Education, DialogTypes } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-pwizard',
  templateUrl: './list-person.component.html',
  styleUrls: ['./list-person.component.css']
})
export class ListPersonComponents implements OnInit {

  institution: any;
  bed: any;
  nurses = [];
  devices = [];
  bedId = -1;
  person:any;
  readonly = false;
  url = '';

  formGroup: FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<ListPersonComponents>,
              private adapter: DateAdapter<any>,
              private fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
   // this.institution = this.rest.getInstitution();
    //this.url = 'data/institution/' + this.institution.id + '/';

    this.person = dialogData.person;
    this.formGroup = fb.group({
      name: [
        { value: this.person.kilo, disabled: true },
        [ ]
      ]
    });
  }


  add0(m){return m<10?'0'+m:m }
  dateformat(date)
  {
//shijianchuo是整数，否则要parseInt转换
    var time = new Date(date);
    //var y = time.getFullYear();
    var m = time.getMonth()+1;
    var d = time.getDate();
    var h = time.getHours();
    var mm = time.getMinutes();
   // var s = time.getSeconds();
    return this.add0(m)+'-'+this.add0(d)+' '+this.add0(h)+':'+this.add0(mm);//+':'+add0(s);
  }

  ngOnInit() {

    this.rest.get('api/influx/getHeartByPersonId?personId=' + this.person.id).subscribe(pdata => {
      console.log(pdata);
      this.person.heart = "--";
      this.person.hearttime = "-----";
      this.person.pressureH = "-";
      this.person.pressureL = "-";
      if(pdata!=null){
        this.person.heart = pdata[2];
        //var date = new Date(pdata[0]).toLocaleString();
        this.person.hearttime = this.dateformat(pdata[0]);
        this.person.pressureH = pdata[3];
        this.person.pressureL = pdata[4];
      }
    },message=>{
      console.error('Failed to get Coordinate By PersonId in: ' + message);
    });
  }
}
