import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DialogTypes } from '../models/wdl-types';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {
  title: string;
  message: string;
  type: DialogTypes;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any, public dialogRef: MatDialogRef<DialogComponent>) {
    this.type = dialogData.type;
    this.title = dialogData.title;
    this.message = dialogData.message;
  }

  isConfirmation() {
    return this.type === DialogTypes.CONFIRMATION;
  }

  getCloseText() {
    return this.isConfirmation() ? 'cancel' : 'close';
  }

  ngOnInit() {
  }
}
