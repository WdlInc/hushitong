import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DraggableModule } from '../draggable/draggable.module';
import { DeviceRoutingModule } from './device-routing.module';
import { FileHelpersModule } from 'ngx-file-helpers';
import { TranslateModule } from '@ngx-translate/core';
import { DeviceComponent } from './device.component';
import { DeviceDialogComponent } from './device-dialog/device-dialog.component';
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from '@angular/material/core';
import { FindDeviceComponent } from './find-device/find-device.component';
import { UploadComponent } from './upload/upload.component';
import { AngularFileUploaderModule } from 'angular-file-uploader';
import { SuimageComponent } from './suimage/suimage.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SbcmaterialModule,
    DraggableModule,
    DeviceRoutingModule,
    FileHelpersModule,
    AngularFileUploaderModule,
    TranslateModule
  ],
  declarations: [
    DeviceComponent,
    DeviceDialogComponent,
    FindDeviceComponent,
    UploadComponent,
    SuimageComponent
  ],
  entryComponents: [
    DeviceDialogComponent,
    FindDeviceComponent,
    UploadComponent,
    SuimageComponent
  ],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }
  ]
})
export class DeviceModule { }
