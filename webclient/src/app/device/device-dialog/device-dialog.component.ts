import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { ActionTypes } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-device-dialog',
  templateUrl: './device-dialog.component.html',
  styleUrls: ['./device-dialog.component.css']
})
export class DeviceDialogComponent implements OnInit {
  formGroup: FormGroup;

  item: any;
  institution: any;
  title = '';

  // could be add, modify
  mode = ActionTypes.ADD;
  disabled = false;

  error = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<DeviceDialogComponent>,
              fb: FormBuilder,
              private translate: TranslateService,
              private rest: RestService) {
    this.institution = dialogData.institution;
    this.mode = dialogData.mode;

    this.title = this.translate.get('add')['value'] + this.translate.get('device')['value'];

    this.formGroup = fb.group({
      deviceId: [
        '',
        [ Validators.required, Validators.minLength(10), Validators.maxLength(16) ]
      ]
    });
  }

  disableSubmit() {
    return this.formGroup.invalid || (this.mode === ActionTypes.MODIFY && !this.formGroup.dirty);
  }

  submit() {
    const data = {};
    _.forEach(this.formGroup.value, (value, key) => {
      data[key] = _.trim(value);
    });

    this.error = '';

    this.rest.getWithParams('data/device/search/deviceId', {deviceId: data['deviceId']}).subscribe(resp => {
      if (resp['_embedded']['devices'].length > 0) {
        this.error = this.translate.get('duplicateDeviceId')['value'];
      } else {
        this.dialogRef.close(data);
      }
    }, () => {
      this.dialogRef.close(data);
    });
  }

  ngOnInit() {
  }
}
