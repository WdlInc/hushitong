import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit {
  uploadConfig: any;

  constructor(public dialogRef: MatDialogRef<UploadComponent>,
              private translate: TranslateService,
              private rest: RestService) {
    this.uploadConfig = {
      multiple: false,
      formatsAllowed: '.bin',
      maxSize: '5',
      uploadAPI: {
        url: this.rest.getUploadUrl(),
        headers: this.rest.getUploadHeaders()
      },
      theme: 'dragNDrop',
      hideProgressBar: false,
      hideResetBtn: true,
      hideSelectBtn: false,
      replaceTexts: {
        selectFileBtn: this.translate.get('selectFile')['value'],
        uploadBtn: this.translate.get('upload')['value'],
        dragNDropBox: this.translate.get('dragAndDrop')['value'],
        afterUploadMsg_success: this.translate.get('uploadSuccess')['value'],
        afterUploadMsg_error: this.translate.get('uploadFail')['value']
      }
    };
  }

  ngOnInit() {
  }
}
