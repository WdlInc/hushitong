import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import { DataService } from './data.service';

import * as Lodash from 'lodash';

@Injectable()
export class WidgetsService {
  public _ = Lodash;

  public allApplications = [{
    type: 'Dashboard',
    route: '/home/dashboard',
    image: '../../assets/images/dashboard.svg',
    module: 'dashboard',
    name: 'Dashboard'
  }];

  constructor(private dataService: DataService) {
  }

  openInfoDialog(title: string, message: string, onClose) {
  }

  openConfirmDialog(title: string, message: string, onClose, onConfirm) {
  }

  openWarningDialog(title: string, primaryMessage: string, secondaryMessage: string, onClose) {
  }

  openErrorDialog(title: string, message1: string, message2: string, onClose) {
  }

  // We set local storage data here since all components need to inject this service.
  cleanupUserStorage() {
    sessionStorage.clear();
  }

  // Save available applications for the current user.
  saveApplications(applications) {
    sessionStorage.setItem('applications', JSON.stringify(applications));
  }

  getApplications() {
    const item = JSON.parse(sessionStorage.getItem('applications'));
    return item ? item : {};
  }

  // Save the current application
  saveApplication(item) {
    sessionStorage.setItem('application', JSON.stringify(item));
  }

  getApplication() {
    const item = JSON.parse(sessionStorage.getItem('application'));
    return item ? item : {};
  }

  getApplicationType() {
    const currentApp = this.getApplication();
    return currentApp.type;
  }

  getApplicationName() {
    const currentApp = this.getApplication();
    return currentApp.name;
  }

  // Save the user info
  saveUser(item) {
    if (item.roleList && !this._.endsWith(item.roleList, '=')) {
      item.roleList = btoa(item.roleList);
    }
    sessionStorage.setItem('user', JSON.stringify(item));
  }

  updateUser(item) {
    const user = this.getUser();
    this._.assign(user, item);

    this.saveUser(user);
  }

  getUser() {
    const item = JSON.parse(sessionStorage.getItem('user'));
    if (item && item.roleList && this._.endsWith(item.roleList, '=')) {
        item.roleList = atob(item.roleList);
    }

    return item ? item : {};
  }

  getUserName() {
    const userInfo = this.getUser();
    return userInfo.name;
  }

  getOamVersion() {
    const userInfo = this.getUser();
    return userInfo.version;
  }

  saveServer(serverInfo) {
    sessionStorage.setItem('server', JSON.stringify(serverInfo));
  }

  getServer(callback) {
    const item = JSON.parse(sessionStorage.getItem('server'));
    if (item) {
      return item;
    } else {
      if (callback) {
        callback();
      }
    }
  }

  private adjustUrl(url) {
    if (!url) {
      return url;
    }

    const fragLen = 45;
    const totalLen = url.length;
    if (totalLen <= fragLen) {
      return url;
    }

    const urls = url.match(/.{45}/g);
    const procLength = urls.length * fragLen;
    if (procLength < totalLen) {
      urls.push(url.substring(procLength));
    }

    return urls.join('- ');
  }

  /**
   * title - show in title
   * error - construct error body
   */
  showHttpError(title, error: any, callback) {
    if (!error) {
      this.openErrorDialog('Internal Error', title, '', callback);
      return;
    }

    if (error instanceof HttpErrorResponse) {
      this.openErrorDialog(error.status + ' - ' + error.statusText, title, this.adjustUrl(error.url), callback);
      return;
    }

   if (error.status) {
     this.openErrorDialog(error.status + ' - ' + error.error, error.message, title, callback);
     return;
   }

   if (error.message) {
     this.openErrorDialog(title, error.message, '', callback);
     return;
   }
  }

  // utilities for grid
  showLoadingOverlay(api) {
    if (api) {
      api.showLoadingOverlay();
    }
  }

  showNoRowsOverlay(api) {
    if (api) {
      api.showNoRowsOverlay();
    }
  }

  hideOverlay(api) {
    if (api) {
      api.hideOverlay();
    }
  }
}
