import { DataService } from './data.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { WidgetsService } from './widgets.service';
import { RestService } from './rest.service';

@Injectable()
export class LoginService {
  allowedModules_R: string[] = [];
  allowedModules_W: string[] = [];
  combinedModule: string[] = ['SystemSettings'];

  user: any;
  idleWindow2NotifyTime = 2 * 60000;

  constructor(public router: Router,
              public widgets: WidgetsService,
              public restService: RestService,
              public dataService: DataService) {
    // Load roleList from sessionStorage, it may have security concern, fix it in the future!
    const userInfo = this.widgets.getUser();
    if (userInfo && userInfo.roleList) {
      this.assginAuthorities(userInfo.roleList);
    }
  }

  public analyzeRoleList(roleList: string) {
  }

  // public assginAuthorities(assignSuccess, roleList)
  public assginAuthorities(roleList) {
    this.analyzeRoleList(roleList);
  }

  public logOut(): void {
    this.logoutSystem();
  }

  private logoutSystem() {
    this.restService.reload2Login();
  }
}
