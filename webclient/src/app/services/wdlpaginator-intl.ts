import { MatPaginator, MatPaginatorIntl } from '@angular/material';
import { Component, Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class WdlPaginatorIntl extends MatPaginatorIntl {
  itemsPerPageLabel = 'Items per page';
  nextPageLabel     = 'Next page';
  previousPageLabel = 'Previous page';
  firstPageLabel = 'First page';
  lastPageLabel = 'Last page';

  pagePrefix = '';
  pageMiddle = ' of ';
  pageSuffix = '';

  constructor(private translateService: TranslateService) {
    super();
    this.itemsPerPageLabel = this.translateService.get('itemsPerPageLabel')['value'];
    this.nextPageLabel = this.translateService.get('nextPageLabel')['value'];
    this.previousPageLabel = this.translateService.get('previousPageLabel')['value'];
    this.firstPageLabel = this.translateService.get('firstPageLabel')['value'];
    this.lastPageLabel = this.translateService.get('lastPageLabel')['value'];
    this.pagePrefix = this.translateService.get('pagePrefix')['value'];
    this.pageMiddle = this.translateService.get('pageMiddle')['value'];
    this.pageSuffix = this.translateService.get('pageSuffix')['value'];
  }

  getRangeLabel = function(page, pageSize, length) {
    if (length === 0 || pageSize === 0) {
      return '0 of ' + length;
    }

    length = Math.max(length, 0);

    const startIndex = page * pageSize;

    // If the start index exceeds the list length, do not try and fix the end index to the end.
    const endIndex = startIndex < length ? Math.min(startIndex + pageSize, length) : startIndex + pageSize;

    return this.pagePrefix + (startIndex + 1) + ' - ' + endIndex + this.pageMiddle + length + this.pageSuffix;
  };
}