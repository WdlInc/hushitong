import { Injectable, EventEmitter, enableProdMode } from '@angular/core';
import { Subject } from 'rxjs/Rx';

@Injectable()
export class DataService {
  private genEventDataSource = new Subject<string>();

  genData$ = this.genEventDataSource.asObservable().share();

  updateGenEventData(data: any) {
    this.genEventDataSource.next(data);
  }
}
