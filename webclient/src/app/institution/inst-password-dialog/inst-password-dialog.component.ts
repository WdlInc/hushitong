import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-inst-password-dialog',
  templateUrl: './inst-password-dialog.component.html',
  styleUrls: ['./inst-password-dialog.component.css']
})
export class InstPasswordDialogComponent {
  formGroup: FormGroup;
  item: any;
  title = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<InstPasswordDialogComponent>,
              fb: FormBuilder,
              private translate: TranslateService) {
    this.item = dialogData.item;

    this.title = this.translate.get("resetPassword")['value'];
    this.formGroup = fb.group({
      instName: [ '', [] ],
      password: [ '', [Validators.required] ]
    });

    this.formGroup.controls.instName.setValue(this.item.instName);
  }

  disableSubmit() {
    return this.formGroup.invalid || !this.formGroup.dirty;
  }

  submit() {
    this.item['password'] = this.formGroup.controls.password.value;
    this.dialogRef.close(this.item);
  }
}