import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { InstitutionRoutingModule } from './institution-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { InstitutionComponent } from './institution.component';
import { InstitutionDialogComponent } from './institution-dialog/institution-dialog.component';
import { InstPasswordDialogComponent } from './inst-password-dialog/inst-password-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DraggableModule,
    InstitutionRoutingModule,
    TranslateModule
  ],
  declarations: [
    InstitutionComponent,
    InstitutionDialogComponent,
    InstPasswordDialogComponent
  ],
  entryComponents: [
    InstitutionDialogComponent,
    InstPasswordDialogComponent
  ]
})
export class InstitutionModule { }
