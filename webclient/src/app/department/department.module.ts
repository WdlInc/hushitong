import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { DepartmentRoutingModule } from './department-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { DepartmentComponent } from './department.component';
import { DepartmentDialogComponent } from './department-dialog/department-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DraggableModule,
    DepartmentRoutingModule,
    TranslateModule
  ],
  declarations: [
    DepartmentComponent,
    DepartmentDialogComponent
  ],
  entryComponents: [
    DepartmentDialogComponent
  ]
})
export class DepartmentModule { }
