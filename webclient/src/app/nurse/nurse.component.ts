import { Component, OnDestroy, OnInit, ViewChild, EventEmitter } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { RestService } from '../services/rest.service';
import { TranslateService } from '@ngx-translate/core';
import { NurseDialogComponent } from './nurse-dialog/nurse-dialog.component';
import { DialogTypes, ActionTypes } from '../models/wdl-types';

import * as _ from 'lodash';

@Component({
  templateUrl: './nurse.component.html',
  styleUrls: ['./nurse.component.css'],
})
export class NurseComponent implements OnInit, OnDestroy {
  url = 'data/nurse';
  institution: any;
  user: any;
  displayedColumns: string[] = ['number', 'nurseName', 'sex', 'departmentName', 'tel', 'actions'];
  dataSource = [];
  searchText = '';

  resultsLength = 0;
  pageSize = 25;
  isLoadingResults = true;

  search: EventEmitter<any> = new EventEmitter();

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog,
              private router: Router,
              private translate: TranslateService,
              private rest: RestService) {
    this.institution = this.rest.getInstitution();
    if (!this.institution.id) {
      console.error('Instituion is invalid: ' + this.institution.id);
    }
    this.user = this.rest.getUser();
    if (!this.isUserQualified()) {
      this.displayedColumns = ['nurseName', 'sex', 'departmentName', 'tel'];
    }
  }

  isUserQualified() {
    return this.user.role === 'header';
  }

  refreshSearch() {
    this.paginator.pageIndex = 0;
    this.search.emit(this.searchText);
  }

  add() {
    this.dialog.open(NurseDialogComponent, {
      disableClose: false,
      autoFocus: false,
      width: '450px',
      data: {
        mode: ActionTypes.ADD,
        institution: this.institution
      }
    }).afterClosed().subscribe(data => {
      if (!data || _.isString(data)) {
        // action was aborted
        return;
      }

      this.rest.post(this.url, data).subscribe((resp) => {
        console.log('Nurse ' + data.departmentName + ' was added.');
        // link it to institution
        this.rest.link(this.url + '/' + resp['id'] + '/institution', 'data/institution/' + this.institution.id).subscribe(() => {
          this.refreshSearch();
        }, () => {
          console.error('Failed to link nurse to institution');
          this.refreshSearch();
        });
      }, message => {
        this.rest.showHttpError('addNurseError', message);
      });
    });
  }

  modify(item) {
    this.dialog.open(NurseDialogComponent, {
      disableClose: false,
      autoFocus: false,
      width: '450px',
      data: {
        mode: ActionTypes.MODIFY,
        item: item,
        institution: this.institution
      }
    }).afterClosed().subscribe(data => {
      if (!data || _.isString(data)) {
        // action was aborted
        return;
      }

      this.rest.patch(this.url + '/' + item.id, data).subscribe(response => {
        this.refreshSearch();
      }, message => {
        this.rest.showHttpError('modifyNurseError', message);
      });
    });
  }

  remove(nurse) {
    this.rest.getWithParams('data/person/search/monitorNurseIn', {
      institutionId: this.institution.id,
      checkoutTime: 1,
      nurseId: nurse.id
    }).subscribe(resp => {
      const persons = resp['_embedded']['persons'];
      if (persons.length > 0) {
        const nonEmptyTitle = this.translate.get('deleteError')['value'];
        this.rest.openGenDialog(DialogTypes.ERROR, nonEmptyTitle, this.translate.get('nonEmptyNurseMessage', {personName: persons[0]['personName']})['value'], null);
      } else {
        const title = this.translate.get('deleteConfirmation')['value'];
        const message = this.translate.get('deleteNurseMessage', {nurseName: nurse.nurseName})['value'];

        this.rest.openGenDialog(DialogTypes.CONFIRMATION, title, message, () => {
          this.rest.delete(this.url + '/' + nurse.id).subscribe(result => {
            this.refreshSearch();
          }, message => {
            this.rest.showHttpError(this.translate.get('error')['value'], message);
          });
        });
      }
    });
  }

  ngOnInit() {
    // If the user changes the sort order, reset back to the first page.
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.search)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;

        const params = {
          institutionId: this.institution.id,
          page: this.paginator.pageIndex ? this.paginator.pageIndex : 0,
          size: this.paginator.pageSize ? this.paginator.pageSize : this.pageSize
        };

        if (this.sort.active && this.sort.direction) {
          params['sort'] = this.sort.active + ',' + this.sort.direction
        }

        if (this.searchText) {
          params['name'] = this.searchText;
          return this.rest.getWithParams(this.url + '/search/name', params);
        } else {
          return this.rest.getWithParams(this.url + '/search/all', params);
        }
      }),
      map(data => {
        this.isLoadingResults = false;
        this.resultsLength = data['page']['totalElements'];

        let number = this.paginator.pageIndex * this.paginator.pageSize;
        const items = data['_embedded']['nurses'];
        items.forEach(item => {
          item['number'] = ++number;
        });

        return items;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        return observableOf([]);
      })
    ).subscribe(data => this.dataSource = data);
  }

  ngOnDestroy() {
  }
}
