import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { NurseRoutingModule } from './nurse-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { NurseComponent } from './nurse.component';
import { NurseDialogComponent } from './nurse-dialog/nurse-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    FormsModule,
    ReactiveFormsModule,
    DraggableModule,
    NurseRoutingModule,
    TranslateModule
  ],
  declarations: [
    NurseComponent,
    NurseDialogComponent
  ],
  entryComponents: [
    NurseDialogComponent
  ]
})
export class NurseModule { }
