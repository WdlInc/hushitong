import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../../services/rest.service';
import { TranslateService } from '@ngx-translate/core';

import * as _ from 'lodash';

@Component({
  selector: 'app-ack-dialog',
  templateUrl: './ack-dialog.component.html',
  styleUrls: ['./ack-dialog.component.css']
})
export class AckDialogComponent implements OnInit {
  institution: any;
  user: any;
  person: any;
  alarms = [];

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<AckDialogComponent>,
              private translate: TranslateService,
              private rest: RestService,
              private dialog: MatDialog) {
    this.institution = this.rest.getInstitution();
    this.user = this.rest.getUser();
    this.person = dialogData.person;
    this.alarms = dialogData.alarms;
  }

  isUserQualified() {
    return this.user.role !== 'director' && this.user.role !== 'doctor';
  }

  getHideAlarmTypeString(alarm) {
    return 'hide' + _.capitalize(alarm) + 'Alarm';
  }

  getAlarmId(alarm) {
    return this.person[alarm.toLowerCase() + 'AlarmId'];
  }

  getAlarmTypeEnableStatus(alarm) {
    if (alarm !== 'UP' && alarm !== 'SIDE' && alarm !== 'AWAY') {
      return true;
    }

    return this.person[alarm.toLowerCase() + 'Alarm'];
  }

  submit(alarmList) {
    alarmList.forEach(item => {
      const alarmId = this.getAlarmId(item.value);
      if (alarmId > 0) {
        this.rest.postWithParams('data/ackAlarm', {alarmId: alarmId}, {}).subscribe(() => {});
      }
    });

    this.dialogRef.close('');
  }

  isAcked(alarm) {
    return this.getAlarmId(alarm) <= 0;
  }

  ngOnInit() {
  }
}
