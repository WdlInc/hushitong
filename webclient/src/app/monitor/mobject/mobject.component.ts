import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { RestService } from '../../services/rest.service';
import { TranslateService } from '@ngx-translate/core';
import { AckDialogComponent } from '../ack-dialog/ack-dialog.component';
import { DataService } from 'app/services/data.service';

import * as _ from 'lodash';

const BEDLINK = '-';

@Component({
  selector: 'app-mobject',
  templateUrl: './mobject.component.html',
  styleUrls: ['./mobject.component.css']
})
export class MobjectComponent implements OnInit {
  @Input() person: any;

  building = '';
  bed = '';
  alarms = [];
  alarmText = '';
  institution: any;
  user: any;

  constructor(private rest: RestService,
              private router: Router,
              private route: ActivatedRoute,
              private dialog: MatDialog,
              private dataService: DataService,
              private translate: TranslateService) {
    this.institution = this.rest.getInstitution();
    this.user = this.rest.getUser();
    this.dataService.genData$.subscribe(data => {
      if (data === 'Person') {
        this.getAlarms();
      }
    });
  }

  getSoundImg() {
    return this.person.silent ? '../../../assets/images/soundoff.png' : '../../../assets/images/sound.png';
  }

  toggleSound() {
    if (this.user.role === 'nurse' || this.user.role === 'header') {
      this.person.silent = !this.person.silent;
      this.rest.patch('data/person/' + this.person.id, {silent: this.person.silent}).subscribe(() => {});
    }
  }

  getBellImg() {
    if (this.alarms.length < 1) {
      return '../../../assets/images/belloff.png';
    }

    for (let i = 0; i < this.alarms.length; i++) {
      if (!this.person['hide' + _.capitalize(this.alarms[i]) + 'Alarm'] && this.getAlarmTypeEnableStatus(this.alarms[i])) {
        return '../../../assets/images/bell.png';
      }
    }

    return '../../../assets/images/belloff.png';
  }

  getColor() {
    if (this.alarms.length < 1) {
      return 'primary';
    }

    for (let i = 0; i < this.alarms.length; i++) {
      if (!this.person['hide' + _.capitalize(this.alarms[i]) + 'Alarm'] && this.getAlarmTypeEnableStatus(this.alarms[i])) {
        return 'warn';
      }
    }

    return 'primary';
  }

  getAlarmString(alarm) {
    let alarmString =
      '<span class="' +
      ((this.person['hide' + _.capitalize(alarm) + 'Alarm'] || !this.getAlarmTypeEnableStatus(alarm)) ? 'acked' : 'unacked') +
      '">';
    alarmString += this.translate.get(alarm)['value'] + '</span>';
    return alarmString;
  }

  getAlarmTypeEnableStatus(alarm) {
    if (alarm !== 'UP' && alarm !== 'SIDE' && alarm !== 'AWAY') {
      return true;
    }

    return this.person[alarm.toLowerCase() + 'Alarm'];
  }

  getAlarms() {
    this.alarms = [];

    if (this.person.deviceAlarmFlag) {
      this.alarms.push('DEVICE');
    }

    if (this.person.awayAlarmFlag) {
      this.alarms.push('AWAY');
    }

    if (this.person.wetAlarmFlag) {
      this.alarms.push('WET');
    }

    if (this.person.sideAlarmFlag) {
      this.alarms.push('SIDE');
    }

    if (this.person.upAlarmFlag) {
      this.alarms.push('UP');
    }

    if (this.person.heartAlarmFlag) {
      this.alarms.push('HEART');
    }

    if (this.person.breatheAlarmFlag) {
      this.alarms.push('BREATHE');
    }

    if (this.person.moveAlarmFlag) {
      this.alarms.push('MOVE');
    }

    if (this.person.ringAlarmFlag) {
      this.alarms.push('RING');
    }

    if (this.person.turnOverReminderAlarmFlag) {
      this.alarms.push('TURN_OVER');
    }

    if (this.person.leftAlarmFlag) {
      this.alarms.push('LEFT');
    }

    if (this.person.rightAlarmFlag) {
      this.alarms.push('RIGHT');
    }

    this.refreshAlarmText();
  }

  refreshAlarmText() {
    if (this.alarms.length < 1) {
      this.alarmText = this.translate.get('normal')['value'];
    } else {
      const alarmStrings = [];
      this.alarms.forEach(alarm => {
        alarmStrings.push(this.getAlarmString(alarm));
      });

      this.alarmText = alarmStrings.join('<span>,&nbsp;</span>');
    }
  }

  openAckDialog() {
    if (this.getColor() === 'primary') {
      return;
    }

    const toBeAcked = [];
    this.alarms.forEach(item => {
      if (item !== 'TURN_OVER') {
        toBeAcked.push(item);
      }
    });

    if (toBeAcked.length === 0) {
      return;
    }

    this.dialog.open(AckDialogComponent, {
      disableClose: false,
      autoFocus: false,
      width: '450px',
      data: {
        person: this.person,
        alarms: toBeAcked
      }
    });
  }

  navToMCenter() {
    this.rest.eventEmit.emit('mcenter');

    this.router.navigate(['mcenter', {
      person: this.person.id,
    }], {relativeTo: this.route.parent.parent});
  }

  ngOnInit() {
    // init person's building, bed number, and alarm info
    if (this.person.bedString) {
      const beds = this.person.bedString.split('##');
      this.building = beds[0];
      beds.shift();
      this.bed = beds.join(BEDLINK);
    }

    this.getAlarms();
  }
}
