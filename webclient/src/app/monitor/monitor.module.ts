import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { HomeModule } from '../home/home.module';
import { MonitorRoutingModule } from './monitor-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { MonitorComponent } from './monitor.component';
import { MobjectComponent } from './mobject/mobject.component';
import { AckDialogComponent } from './ack-dialog/ack-dialog.component';
import { SafeHtmlPipe } from '../services/safeHtml.pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SbcmaterialModule,
    DraggableModule,
    InfiniteScrollModule,
    HomeModule,
    MonitorRoutingModule,
    TranslateModule
  ],
  declarations: [
    MonitorComponent,
    MobjectComponent,
    AckDialogComponent,
    SafeHtmlPipe
  ],
  entryComponents: [
    AckDialogComponent
  ]
})
export class MonitorModule { }
