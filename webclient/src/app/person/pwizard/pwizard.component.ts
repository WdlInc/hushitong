import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { SexEnums, Education, DialogTypes } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-pwizard',
  templateUrl: './pwizard.component.html',
  styleUrls: ['./pwizard.component.css']
})
export class PwizardComponent implements OnInit {
  basicForm: FormGroup;
  contactForm: FormGroup;
  healthForm: FormGroup;
  hospitalForm: FormGroup;

  institution: any;
  bed: any;
  nurses = [];
  devices = [];
  bedId = -1;

  readonly = false;
  url = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PwizardComponent>,
              private adapter: DateAdapter<any>,
              fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.institution = this.rest.getInstitution();
    this.url = 'data/institution/' + this.institution.id + '/';

    switch (this.translate.currentLang) {
    case 'en':
      this.adapter.setLocale('en');
      break;
    case 'tw':
      this.adapter.setLocale('zh-TW');
      break;
    case 'zh':
    default:
      this.adapter.setLocale('zh-CN');
      break;
    }

    this.basicForm = fb.group({
      personName: [
        { value: '', disabled: this.readonly },
        [ Validators.required, Validators.minLength(1), Validators.maxLength(40) ]
      ],
      sex: [
        { value: 'male', disabled: this.readonly },
        [ Validators.required, ]
      ],
      nation: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      education: [
        { value: 'none', disabled: this.readonly },
        []
      ],
      rid: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      birth: [
        { value: null, disabled: this.readonly },
        []
      ],
      phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
    });

    this.contactForm = fb.group({
      contact1Name: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(40) ]
      ],
      contact1Sex: [
        { value: 'male', disabled: this.readonly },
        [ ]
      ],
      contact1Phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      contact1Address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
      contact2Name: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(40) ]
      ],
      contact2Sex: [
        { value: 'male', disabled: this.readonly },
        []
      ],
      contact2Phone: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      contact2Address: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
    });

    this.healthForm = fb.group({
      heartUp: [
        { value: 90, disabled: this.readonly },
        []
      ],
      heartLow: [
        { value: 40, disabled: this.readonly },
        []
      ],
      breatheUp: [
        { value: 25, disabled: this.readonly },
        []
      ],
      breatheLow: [
        { value: 10, disabled: this.readonly },
        []
      ],
      move: [
        { value: 10, disabled: this.readonly },
        []
      ],
      turnOverReminder: [
        { value: 0, disabled: this.readonly },
        []
      ],
      awayAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      upAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      sideAlarm: [
        { value: true, disabled: this.readonly },
        []
      ],
      silent: [
        { value: false, disabled: this.readonly },
        []
      ],
    });

    this.hospitalForm = fb.group({
      checkinRecord: [
        { value: '', disabled: this.readonly },
        [ Validators.maxLength(256) ]
      ],
      nurseId: [
        { value: null, disabled: this.readonly },
        []
      ],
      device1Id: [
        { value: null, disabled: this.readonly },
        []
      ],
    });
  }

  bedSelectionChange(bed) {
    this.bed = bed;
  }

  submit() {
    const data = {};
    _.forEach(this.basicForm.value, (value, key) => {
      if (key === 'sex') {
        data[key] = value === 'male' ? SexEnums.MALE : SexEnums.FEMAIL;
      } else if (key === 'education') {
        switch (value) {
        case 'none':
          data[key] = Education.NONE;
          break;
        case 'primary':
          data[key] = Education.PRIMARY;
          break;
        case 'middle':
          data[key] = Education.MIDDLE;
          break;
        case 'college':
          data[key] = Education.COLLEGE;
          break;
        }
      } else if (key === 'birth') {
        data[key] = value ? value.getTime() : null;
      } else {
        data[key] = _.trim(value);
      }
    });

    _.forEach(this.contactForm.value, (value, key) => {
      if (key.endsWith('Sex')) {
        data[key] = value === 'male' ? SexEnums.MALE : SexEnums.FEMAIL;
      } else {
        data[key] = _.trim(value);
      }
    });

    _.forEach(this.healthForm.value, (value, key) => {
      data[key] = value;
    });

    _.forEach(this.hospitalForm.value, (value, key) => {
      data[key] = _.trim(value);
    });

    if (this.bed && this.bed.id > -1) {
      data['bedId'] = this.bed.id;
      data['bedString'] = this.bed.nameString;
    }

    if (data['nurseId'] > -1) {
      const index = _.findIndex(this.nurses, {id: _.toNumber(data['nurseId'])});
      if (index > -1) {
        data['nurseName'] = this.nurses[index]['nurseName'];
      }
    }

    data['checkinTime'] = new Date().getTime();
    data['checkoutTime'] = 0;

    data['kilo']=0;
    data['longitude']=0;
    data['latitude']=0;
    if (data['device1Id']) {
      const index = _.findIndex(this.devices, {deviceId: data['device1Id']});
      if (index > -1) {
        const select = this.devices[index];
        data['bind1Time'] = new Date().getTime();
        data['device1'] = select.id;
      }
    }

    this.dialogRef.close(data);
  }

  ngOnInit() {
    this.rest.get(this.url + 'nurses').subscribe(data => {
      this.nurses = data['_embedded']['nurses'];
    });

    this.rest.get(this.url + 'devices').subscribe(data => {
      const devices = data['_embedded']['devices'];
      this.devices = [];
      devices.forEach(device => {
        if (device.personId < 1) {
          this.devices.push(device);
        }
      });
    });
  }
}
