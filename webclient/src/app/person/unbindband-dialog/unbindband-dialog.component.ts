import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../../services/rest.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-unbindband-dialog',
  templateUrl: './unbindband-dialog.component.html',
  styleUrls: ['./unbindband-dialog.component.css']
})
export class UnbindBandDialogComponent implements OnInit {
  formGroup: FormGroup;
  person: any;
  url = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<UnbindBandDialogComponent>,
              private fb: FormBuilder) {
    this.person = this.dialogData.person;

    this.formGroup = fb.group({
      name: [
        { value: this.person.personName, disabled: true },
        [ ]
      ],
      device2: [
        { value: this.person.device2Id, disabled: !this.person.device2Id },
        [ ]
      ],
    });
  }

  submit() {
    const data = {};

    if (this.person.device2Id && this.formGroup.controls.device2.value) {
      data['device2'] = this.person.device2;
    }

    this.dialogRef.close(data);
  }

  ngOnInit() {
  }
}
