import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { ActionTypes } from '../../models/wdl-types';

@Component({
  selector: 'app-person-dialog',
  templateUrl: './person-dialog.component.html',
  styleUrls: ['./person-dialog.component.css']
})
export class PersonDialogComponent implements OnInit {
  basicForm: FormGroup;
  contactForm: FormGroup;
  healthForm: FormGroup;
  hospitalForm: FormGroup;
  deviceForm: FormGroup;

  person: any;
  readonly = true;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PersonDialogComponent>,
              private adapter: DateAdapter<any>,
              fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.person = dialogData.person;

    this.basicForm = fb.group({
      name: [
        { value: this.person.personName, disabled: this.readonly },
      ],
      sex: [
        { value: this.getStringFromEnum(this.person.sex), disabled: this.readonly },
        [ Validators.required, ]
      ],
      nation: [
        { value: this.getString(this.person.nation), disabled: this.readonly },
        []
      ],
      education: [
        { value: this.getStringFromEnum(this.person.education.toLowerCase()), disabled: this.readonly },
        []
      ],
      rid: [
        { value: this.getString(this.person.rid), disabled: this.readonly },
        [ Validators.required ]
      ],
      birth: [
        { value: this.getTimeString(this.person.birth), disabled: this.readonly },
        []
      ],
      phone: [
        { value: this.getString(this.person.phone), disabled: this.readonly },
        []
      ],
      address: [
        { value: this.getString(this.person.address), disabled: this.readonly },
        []
      ],
    });

    this.contactForm = fb.group({
      name1: [
        { value: this.getString(this.person.contact1Name), disabled: this.readonly },
        [ ]
      ],
      sex1: [
        { value: this.getStringFromEnum(this.person.contact1Sex), disabled: this.readonly },
        [ ]
      ],
      phone1: [
        { value: this.getString(this.person.contact1Phone), disabled: this.readonly },
        []
      ],
      address1: [
        { value: this.getString(this.person.contact1Address), disabled: this.readonly },
        []
      ],
      name2: [
        { value: this.getString(this.person.contact2Name), disabled: this.readonly },
        [ ]
      ],
      sex2: [
        { value: this.getStringFromEnum(this.person.contact2Sex), disabled: this.readonly },
        [ ]
      ],
      phone2: [
        { value: this.getString(this.person.contact2Phone), disabled: this.readonly },
        []
      ],
      address2: [
        { value: this.getString(this.person.contact2Address), disabled: this.readonly },
        []
      ],
    });

    this.healthForm = fb.group({
      heartUp: [
        { value: this.person.heartUp, disabled: this.readonly },
        []
      ],
      heartLow: [
        { value: this.person.heartLow, disabled: this.readonly },
        []
      ],
      breatheUp: [
        { value: this.person.breatheUp, disabled: this.readonly },
        []
      ],
      breatheLow: [
        { value: this.person.breatheLow, disabled: this.readonly },
        []
      ],
      move: [
        { value: this.person.move, disabled: this.readonly },
        []
      ],
      turnOverReminder: [
        { value: this.person.turnOverReminder, disabled: this.readonly },
        []
      ],
      awayAlarm: [
        { value: this.person.awayAlarm, disabled: this.readonly },
        []
      ],
      upAlarm: [
        { value: this.person.upAlarm, disabled: this.readonly },
        []
      ],
      sideAlarm: [
        { value: this.person.sideAlarm, disabled: this.readonly },
        []
      ],
      silent: [
        { value: this.person.silent, disabled: this.readonly },
        []
      ],
    });

    this.hospitalForm = fb.group({
      bed: [
        { value: this.getBedString(this.person.bedString), disabled: true },
        []
      ],
      checkinTime: [
        { value: this.getTimeString(this.person.checkinTime), disabled: true },
        []
      ],
      checkinRecord: [
        { value: this.getString(this.person.checkinRecord), disabled: this.readonly },
        []
      ],
      checkoutTime: [
        { value: this.getTimeString(this.person.checkoutTime), disabled: true },
        []
      ],
      checkoutRecord: [
        { value: this.getString(this.person.checkoutRecord), disabled: this.readonly },
        []
      ],
      nurse: [
        { value: this.getString(this.person.nurseName), disabled: this.readonly },
        []
      ],
    });

    this.deviceForm = fb.group({
      device1: [
        { value: this.getString(this.person.device1Id), disabled: this.readonly },
        []
      ],
      bind1Time: [
        { value: this.getTimeString(this.person.bind1Time), disabled: true },
        []
      ],
      unbind1Time: [
        { value: this.getTimeString(this.person.unbind1Time), disabled: true },
        []
      ],
      device2: [
        { value: this.getString(this.person.device2Id), disabled: this.readonly },
        []
      ],
      bind2Time: [
        { value: this.getTimeString(this.person.bind2Time), disabled: true },
        []
      ],
      unbind2Time: [
        { value: this.getTimeString(this.person.unbind2Time), disabled: true },
        []
      ]
    });
  }

  getString(input) {
    return input ? input : '-';
  }

  getTimeString(input) {
    if (input > 0) {
      return new Date(input).toLocaleString();
    }

    return '-';
  }

  getBedString(input) {
    if (!input) {
      return '-';
    }

    return input.replace(/##/g, ' - ');
  }

  getStringFromEnum(input) {
    return input ? this.translate.get(input)['value'] : '-';
  }

  ngOnInit() {
  }
}
