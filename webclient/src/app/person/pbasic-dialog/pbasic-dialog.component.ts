import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';
import { ActionTypes } from '../../models/wdl-types';
import { SexEnums, Education } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-pbasic-dialog',
  templateUrl: './pbasic-dialog.component.html',
  styleUrls: ['./pbasic-dialog.component.css']
})
export class PbasicDialogComponent implements OnInit {
  basicForm: FormGroup;
  person: any;
  readonly = false;
  mode = ActionTypes.MODIFY;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PbasicDialogComponent>,
              private adapter: DateAdapter<any>,
              fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.person = dialogData.person;

    switch (this.translate.currentLang) {
    case 'en':
      this.adapter.setLocale('en');
      break;
    case 'tw':
      this.adapter.setLocale('zh-TW');
      break;
    case 'zh':
    default:
      this.adapter.setLocale('zh-CN');
      break;
    }

    this.basicForm = fb.group({
      personName: [
        { value: this.person.personName, disabled: this.readonly },
        [ Validators.required, Validators.minLength(1), Validators.maxLength(40) ]
      ],
      sex: [
        { value: this.getSexFromEnum(this.person.sex), disabled: this.readonly },
        [ Validators.required, ]
      ],
      nation: [
        { value: this.person.nation, disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      education: [
        { value: this.getEducationFromEnum(this.person.education), disabled: this.readonly },
        []
      ],
      rid: [
        { value: this.person.rid, disabled: this.readonly },
        [ Validators.maxLength(20) ]
      ],
      birth: [
        { value: this.getTime(this.person.birth), disabled: this.readonly },
        []
      ],
      phone: [
        { value: this.person.phone, disabled: this.readonly },
        [ Validators.maxLength(12) ]
      ],
      address: [
        { value: this.person.address, disabled: this.readonly },
        [ Validators.maxLength(128) ]
      ],
    });
  }

  getTime(input) {
    if (input > 0) {
      return new Date(input);
    }

    return null;
  }

  getSexFromEnum(input) {
    return input === 'MALE' ? 'male' : 'female';
  }

  getEducationFromEnum(input) {
    switch (input) {
    case 'NONE':
      return 'none';
    case 'PRIMARY':
      return 'primary';
    case 'MIDDLE':
      return 'middle';
    case 'COLLEGE':
      return 'college';
    default:
      return 'none';
    }
  }

  submit() {
    const data = {};
    _.forEach(this.basicForm.value, (value, key) => {
      if (key === 'sex') {
        data[key] = value === 'male' ? SexEnums.MALE : SexEnums.FEMAIL;
      } else if (key === 'education') {
        switch (value) {
        case 'none':
          data[key] = Education.NONE;
          break;
        case 'primary':
          data[key] = Education.PRIMARY;
          break;
        case 'middle':
          data[key] = Education.MIDDLE;
          break;
        case 'college':
          data[key] = Education.COLLEGE;
          break;
        }
      } else if (key === 'birth') {
        data[key] = value ? value.getTime() : null;
      } else {
        data[key] = _.trim(value);
      }
    });

    this.dialogRef.close(data);
  }

  ngOnInit() {
  }
}
