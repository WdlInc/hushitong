import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-ptype-dialog',
  templateUrl: './ptype-dialog.component.html',
  styleUrls: ['./ptype-dialog.component.css']
})
export class PtypeDialogComponent implements OnInit {
  types = [];
  selection = '';
  type = 'in';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any, public dialogRef: MatDialogRef<PtypeDialogComponent>) {
    if (this.dialogData.type) {
      this.type = this.dialogData.type;
    }

    this.types.push('basicInfo');
    this.types.push('contactInfo');

    if (this.type !== 'out') {
      this.types.push('healthSetting');
    }

    this.types.push('inHospitalInfo');

    this.selection = this.types[0];
  }

  submit() {
    this.dialogRef.close(this.selection);
  }

  ngOnInit() {
  }
}
