import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-checkout-dialog',
  templateUrl: './checkout-dialog.component.html',
  styleUrls: ['./checkout-dialog.component.css']
})
export class CheckoutDialogComponent implements OnInit {
  formGroup: FormGroup;
  person: any;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<CheckoutDialogComponent>,
              private fb: FormBuilder) {
    this.person = this.dialogData.person;
    this.person.sex = this.person.sex === 'MALE' ? 'male' : 'female';

    this.formGroup = fb.group({
      name: [
        { value: this.person.personName, disabled: true },
        [ ]
      ],
      sex: [
        { value: this.person.sex, disabled: true },
        [ ]
      ],
      rid: [
        { value: this.person.rid, disabled: true },
        [ ]
      ],
      checkoutRecord: [
        { value: this.person.checkoutRecord, disabled: false },
        [ Validators.maxLength(256) ]
      ]
    });
  }

  submit() {
    this.dialogRef.close(this.formGroup.value);
  }

  ngOnInit() {
  }
}