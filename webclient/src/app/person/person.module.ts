import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SbcmaterialModule } from './../sbcmaterial/sbcmaterial.module';
import { DraggableModule } from '../draggable/draggable.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PersonRoutingModule } from './person-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { ArchwizardModule } from 'angular-archwizard';
import { PersonComponent } from './person.component';
import { PersonDialogComponent } from './person-dialog/person-dialog.component';
import { BindDialogComponent } from './bind-dialog/bind-dialog.component';
import { BindBandDialogComponent } from './bindband-dialog/bindband-dialog.component';
import { CheckoutDialogComponent } from './checkout-dialog/checkout-dialog.component';
import { HomeModule } from '../home/home.module';
import { PwizardComponent } from './pwizard/pwizard.component';
import { PersoninComponent } from './personin/personin.component';
import { PersonoutComponent } from './personout/personout.component';
import { UnbindDialogComponent } from './unbind-dialog/unbind-dialog.component';
import { UnbindBandDialogComponent} from './unbindband-dialog/unbindband-dialog.component';
import { PbasicDialogComponent } from './pbasic-dialog/pbasic-dialog.component';
import { PtypeDialogComponent } from './ptype-dialog/ptype-dialog.component';
import { PcontactDialogComponent } from './pcontact-dialog/pcontact-dialog.component';
import { PhealthDialogComponent } from './phealth-dialog/phealth-dialog.component';
import { PhospitalDialogComponent } from './phospital-dialog/phospital-dialog.component';
import { McenterModule } from '../mcenter/mcenter.module';

@NgModule({
  imports: [
    CommonModule,
    SbcmaterialModule,
    DraggableModule,
    FormsModule,
    ReactiveFormsModule,
    PersonRoutingModule,
    ArchwizardModule,
    TranslateModule,
    HomeModule,
    McenterModule
  ],
  declarations: [
    PersonComponent,
    PersonDialogComponent,
    CheckoutDialogComponent,
    BindDialogComponent,
    PwizardComponent,
    PersoninComponent,
    PersonoutComponent,
    UnbindDialogComponent,
    PbasicDialogComponent,
    PtypeDialogComponent,
    PcontactDialogComponent,
    PhealthDialogComponent,
    PhospitalDialogComponent,
    UnbindBandDialogComponent,
    BindBandDialogComponent
  ],
  entryComponents: [
    PersonDialogComponent,
    CheckoutDialogComponent,
    BindDialogComponent,
    PwizardComponent,
    UnbindDialogComponent,
    PbasicDialogComponent,
    PcontactDialogComponent,
    PtypeDialogComponent,
    PhealthDialogComponent,
    PhospitalDialogComponent,
    UnbindBandDialogComponent,
    BindBandDialogComponent
  ]
})
export class PersonModule { }
