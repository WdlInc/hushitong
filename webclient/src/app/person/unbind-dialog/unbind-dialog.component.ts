import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../../services/rest.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-unbind-dialog',
  templateUrl: './unbind-dialog.component.html',
  styleUrls: ['./unbind-dialog.component.css']
})
export class UnbindDialogComponent implements OnInit {
  formGroup: FormGroup;
  person: any;
  url = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<UnbindDialogComponent>,
              private fb: FormBuilder) {
    this.person = this.dialogData.person;

    this.formGroup = fb.group({
      name: [
        { value: this.person.personName, disabled: true },
        [ ]
      ],
      device1: [
        { value: this.person.device1Id, disabled: !this.person.device1Id },
        [ ]
      ],
    });
  }

  submit() {
    const data = {};

    if (this.person.device1Id && this.formGroup.controls.device1.value) {
      data['device1'] = this.person.device1;
    }

    this.dialogRef.close(data);
  }

  ngOnInit() {
  }
}