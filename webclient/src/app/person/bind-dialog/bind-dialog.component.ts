import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { RestService } from '../../services/rest.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-bind-dialog',
  templateUrl: './bind-dialog.component.html',
  styleUrls: ['./bind-dialog.component.css']
})
export class BindDialogComponent implements OnInit {
  devices = [];
  formGroup: FormGroup;
  institution: any;
  person: any;
  url = '';

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<BindDialogComponent>,
              private rest: RestService,
              private fb: FormBuilder) {
    this.institution = this.rest.getInstitution();
    this.url = 'data/institution/' + this.institution.id + '/';

    this.person = this.dialogData.person;

    this.formGroup = fb.group({
      name: [
        { value: this.person.personName, disabled: true },
        [ ]
      ],
      device1: [
        { value: this.person.device1Id, disabled: false },
        [ ]
      ]
    });
  }

  submit() {
    const data = {};
    const newDevice1Id = this.formGroup.controls.device1.value;

    if (this.person.device1Id != newDevice1Id) {
      data['device1Id'] = newDevice1Id;
      data['bind1Time'] = new Date().getTime();
      data['unbind1Time'] = 0;
      const index = _.findIndex(this.devices, {deviceId: newDevice1Id});
      if (index > -1) {
        data['device1'] = this.devices[index]['id'];
      }
    }

    this.dialogRef.close(data);
  }

  ngOnInit() {
    this.rest.get(this.url + 'devices').subscribe(data => {
      const devices = data['_embedded']['devices'];
      this.devices = [];
      devices.forEach(device => {
        if (device.personId < 1 || device.deviceId === this.person.device1Id) {
          this.devices.push(device);
        }
      });
    });
  }
}