import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DateAdapter } from '@angular/material/core';
import { TranslateService } from '@ngx-translate/core';
import { RestService } from '../../services/rest.service';

import * as _ from 'lodash';

@Component({
  selector: 'app-phospital-dialog',
  templateUrl: './phospital-dialog.component.html',
  styleUrls: ['./phospital-dialog.component.css']
})
export class PhospitalDialogComponent implements OnInit {
  hospitalForm: FormGroup;
  institution: any;
  person: any;
  nurses = [];
  readonly = false;
  bedId = -1;
  bed: any;
  url = "";

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PhospitalDialogComponent>,
              private adapter: DateAdapter<any>,
              fb: FormBuilder,
              private rest: RestService,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.institution = this.rest.getInstitution();
    this.url = 'data/institution/' + this.institution.id + '/';
    this.rest.get(this.url + 'nurses').subscribe(data => {
      this.nurses = data['_embedded']['nurses'];
    });

    this.person = dialogData.person;
    this.bedId = this.person.bedId;

    this.hospitalForm = fb.group({
      checkinTime: [
        { value: this.getTimeString(this.person.checkinTime), disabled: true },
        []
      ],
      checkinRecord: [
        { value: this.person.checkinRecord, disabled: this.readonly },
        [ Validators.maxLength(256) ]
      ],
      checkoutTime: [
        { value: this.getTimeString(this.person.checkoutTime), disabled: true },
        []
      ],
      checkoutRecord: [
        { value: this.person.checkoutRecord, disabled: this.readonly },
        [ Validators.maxLength(256) ]
      ],
      nurse: [
        { value: this.person.nurseId, disabled: this.readonly },
        []
      ],
    });
  }

  getTimeString(input) {
    if (input > 0) {
      return new Date(input).toLocaleString();
    }

    return '-';
  }

  bedSelectionChange(bed) {
    this.bed = bed;
    this.hospitalForm.markAsDirty();
  }

  submit() {
    const data: any = {};

    data.checkinRecord = _.trim(this.hospitalForm.controls.checkinRecord.value);
    data.checkoutRecord = _.trim(this.hospitalForm.controls.checkoutRecord.value);

    if (this.bed && this.bed.id > -1) {
      data.bedId = this.bed.id;
      data.bedString = this.bed.nameString;
    }

    const nurseId = this.hospitalForm.controls.nurse.value ? _.toNumber(this.hospitalForm.controls.nurse.value) : 0;
    if (nurseId > 0 && nurseId !== this.person.nurseId) {
      data.nurseId = nurseId;
      const index = _.findIndex(this.nurses, {id: nurseId});
      if (index > -1) {
        data.nurseName = this.nurses[index]['nurseName'];
      }
    }

    this.dialogRef.close(data);
  }

  ngOnInit() {
  }
}