import { Component, OnDestroy, OnInit, EventEmitter, ViewChild, Input, SimpleChanges } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MatPaginator, MatSort } from '@angular/material';
import { merge, of as observableOf } from 'rxjs';
import { catchError, map, startWith, switchMap } from 'rxjs/operators';
import { RestService } from '../../services/rest.service';
import { PtypeDialogComponent } from '../ptype-dialog/ptype-dialog.component'
import { PersonDialogComponent } from '../person-dialog/person-dialog.component'
import { PbasicDialogComponent } from '../pbasic-dialog/pbasic-dialog.component'
import { PcontactDialogComponent } from '../pcontact-dialog/pcontact-dialog.component'
import { PhealthDialogComponent } from '../phealth-dialog/phealth-dialog.component'
import { PhospitalDialogComponent } from '../phospital-dialog/phospital-dialog.component'
import { PersonDetailsComponent } from '../../mcenter/person-details/person-details.component';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs/Observable';
import { DialogTypes, ActionTypes } from '../../models/wdl-types';

import * as _ from 'lodash';

@Component({
  selector: 'app-personout',
  templateUrl: './personout.component.html',
  styleUrls: ['./personout.component.css'],
})
export class PersonoutComponent implements OnInit, OnDestroy {
  @Input() selectionChange: any;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: string[] = ['number', 'name', 'sex', 'rid', 'building', 'level', 'room', 'bed', 'nurse', 'checkinTime', 'checkoutTime', 'actions'];

  dataSource = [];
  searchText = '';
  resultsLength = 0;
  pageSize = 25;
  search: EventEmitter<any> = new EventEmitter();

  url = 'data/person';
  isLoadingResults = true;
  institution: any;
  user: any;

  constructor(public dialog: MatDialog,
              private translate: TranslateService,
              private rest: RestService) {
    this.institution = this.rest.getInstitution();
    this.user = this.rest.getUser();
  }

  isUserQualified() {
    return this.user.role === 'header';
  }

  remove(person) {
    const title = this.translate.get('deleteConfirmation')['value'];
    const message = this.translate.get('deletePersonMessage', {personName: person.personName})['value'];

    this.rest.openGenDialog(DialogTypes.CONFIRMATION, title, message, () => {
      this.rest.delete(this.url + '/' + person.id).subscribe(result => {
        console.info('Person ' + person.personName + ' was removed.');
        this.refreshSearch();
      }, message => {
        this.rest.showHttpError(this.translate.get('error')['value'], message);
        this.refreshSearch();
      });
    });
  }

  view(person) {
    this.dialog.open(PersonDialogComponent, {
      disableClose: false,
      autoFocus: false,
      width: '840px',
      data: {
        mode: ActionTypes.READ,
        person: person
      }
    });
  }

  modify(person) {
    this.dialog.open(PtypeDialogComponent, {
      disableClose: true,
      autoFocus: false,
      width: '400px',
      data: {
        type: 'out'
      }
    }).afterClosed().subscribe(data => {
      if (!data) {
        return;
      }

      switch (data) {
      case 'basicInfo':
        return this.modifyBasicInfo(person);
      case 'contactInfo':
        return this.modifyContactInfo(person);
      case 'inHospitalInfo':
        return this.modifyInHospitalInfo(person);
      }
    });
  }

  modifyBasicInfo(person) {
    this.dialog.open(PbasicDialogComponent, {
      disableClose: true,
      autoFocus: false,
      width: '600px',
      data: {
        person: person
      }
    }).afterClosed().subscribe(data => {
      if (!data || _.isString(data)) {
        return;
      }

      this.rest.patch(this.url + '/' + person.id, data).subscribe(resp => {
        console.log('update basic info for ' + person.personName + ' successfully!');
        this.refreshSearch();
      }, message => {
        this.rest.showHttpError('modifyPersonError', message);
        this.refreshSearch();
      });
    });
  }

  modifyContactInfo(person) {
    this.dialog.open(PcontactDialogComponent, {
      disableClose: true,
      autoFocus: false,
      width: '600px',
      data: {
        person: person
      }
    }).afterClosed().subscribe(data => {
      if (!data || _.isString(data)) {
        return;
      }

      this.rest.patch(this.url + '/' + person.id, data).subscribe(resp => {
        console.log('update contact info for ' + person.personName + ' successfully!');
        this.refreshSearch();
      }, message => {
        this.rest.showHttpError('modifyPersonError', message);
        this.refreshSearch();
      });
    });
  }

  modifyInHospitalInfo(person) {
    this.dialog.open(PhospitalDialogComponent, {
      disableClose: true,
      autoFocus: false,
      width: '800px',
      data: {
        person: person
      }
    }).afterClosed().subscribe(data => {
      if (!data || _.isString(data)) {
        return;
      }

      const personId = person.id;
      const observableBatch = [];

      this.rest.patch(this.url + '/' + person.id, data).subscribe((resp) => {
        console.log('Modify person hospital ' + data.personName + ' successfully!');
        this.refreshSearch();
      }, message => {
        this.rest.showHttpError('ModifyPersonError', message);
        this.refreshSearch();
      });
    });
  }

  viewHist(person) {
    this.dialog.open(PersonDetailsComponent, {
      disableClose: true,
      autoFocus: false,
      width: '1200px',
      data: {
        person: person
      }
    });
  }

  refreshSearch() {
    this.paginator.pageIndex = 0;
    this.search.emit(this.searchText);
  }

  ngOnInit() {
  }

  initPersion() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

    merge(this.sort.sortChange, this.paginator.page, this.search)
    .pipe(
      startWith({}),
      switchMap(() => {
        this.isLoadingResults = true;

        const params = {
          institutionId: this.institution.id,
          checkoutTime: 1,
          page: this.paginator.pageIndex ? this.paginator.pageIndex : 0,
          size: this.paginator.pageSize ? this.paginator.pageSize : this.pageSize
        };

        if (this.sort.active && this.sort.direction) {
          params['sort'] = this.sort.active + ',' + this.sort.direction
        }

        if (this.searchText) {
          params['name'] = this.searchText;
          return this.rest.getWithParams(this.url + '/search/nameout', params);
        } else {
          return this.rest.getWithParams(this.url + '/search/allout', params);
        }
      }),
      map(data => {
        this.isLoadingResults = false;
        this.resultsLength = data['page']['totalElements'];
        const persons = data['_embedded']['persons'];
        let number = this.paginator.pageIndex * this.paginator.pageSize;

        persons.forEach(person => {
          person['number'] = ++number;

          if (person['bedString']) {
            const bedNames = person['bedString'].split('##');
            person['building'] = bedNames.length > 0 ? bedNames[0] : '';
            person['level'] = bedNames.length > 1 ? bedNames[1] : '';
            person['room'] = bedNames.length > 2 ? bedNames[2] : '';
            person['bed'] = bedNames.length > 3 ? bedNames[3] : '';
          } else {
            person['building'] = '';
            person['level'] = '';
            person['room'] = '';
            person['bed'] = '';
          }
        });

        return persons;
      }),
      catchError(() => {
        this.isLoadingResults = false;
        return observableOf([]);
      })
    ).subscribe(data => this.dataSource = data);
  }

  getTimeString(input) {
    if (input === 0) {
      return '-';
    } else {
      return new Date(input).toLocaleString();
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes && changes.selectionChange && changes.selectionChange.currentValue === 1) {
      this.initPersion();
    }
  }

  ngOnDestroy() {
  }
}
