import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

import * as _ from 'lodash';

@Component({
  selector: 'app-phealth-dialog',
  templateUrl: './phealth-dialog.component.html',
  styleUrls: ['./phealth-dialog.component.css']
})
export class PhealthDialogComponent implements OnInit {
  healthForm: FormGroup;
  person: any;
  readonly = false;

  constructor(@Inject(MAT_DIALOG_DATA) private dialogData: any,
              public dialogRef: MatDialogRef<PhealthDialogComponent>,
              fb: FormBuilder,
              private translate: TranslateService,
              private dialog: MatDialog) {
    this.person = dialogData.person;

    this.healthForm = fb.group({
      heartUp: [
        { value: this.person.heartUp, disabled: this.readonly },
        []
      ],
      heartLow: [
        { value: this.person.heartLow, disabled: this.readonly },
        []
      ],
      breatheUp: [
        { value: this.person.breatheUp, disabled: this.readonly },
        []
      ],
      breatheLow: [
        { value: this.person.breatheLow, disabled: this.readonly },
        []
      ],
      move: [
        { value: this.person.move, disabled: this.readonly },
        []
      ],
      turnOverReminder: [
        { value: this.person.turnOverReminder, disabled: this.readonly },
        []
      ],
      awayAlarm: [
        { value: this.person.awayAlarm, disabled: this.readonly },
        []
      ],
      upAlarm: [
        { value: this.person.upAlarm, disabled: this.readonly },
        []
      ],
      sideAlarm: [
        { value: this.person.sideAlarm, disabled: this.readonly },
        []
      ],
      silent: [
        { value: this.person.silent, disabled: this.readonly },
        []
      ],
    });
  }

  submit() {
    this.dialogRef.close(this.healthForm.value);
  }

  ngOnInit() {
  }
}
