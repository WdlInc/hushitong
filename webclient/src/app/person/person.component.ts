import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { RestService } from '../services/rest.service';

import * as _ from 'lodash';

@Component({
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.css'],
})
export class PersonComponent implements OnInit, OnDestroy {
  tabIndex: any;

  constructor(public dialog: MatDialog,
              private router: Router,
              private rest: RestService) {
  }

  selectedTabChange(event) {
    this.tabIndex = event.index;
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }
}
